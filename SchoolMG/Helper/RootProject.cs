﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SchoolMG.Helper
{
    class RootProject
    {
        //
        // Get Root Project Directory
        //
        static public string ProjectPath { get => Path.GetFullPath(@"../.."); }
    }
}
