﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.Helper
{
    class DbHelper
    {
        static string ConnectionStirng { get => @"Data source=.\SQLEXPRESS; Initial Catalog = SchoolDB; User Id=sa; password=sa"; }
        static  SqlConnection connection ;

        static public bool IsConnected
        {
            get
            {
                try
                {
                    Open();
                    Console.WriteLine("Connected Successfully");
                    Close();
                    return true;
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Connected fail: {0}", e.ToString());
                    return false;
                }
            }
        }


        static DbHelper()
        {
            connection = new SqlConnection(ConnectionStirng);
        }

        public static DataTable GetData(string sqlCmd)
        {
            Open();
            SqlCommand sqlCommand = new SqlCommand(sqlCmd, connection);
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
            sqlDataAdapter.Fill(dataTable);
            Close();
            return dataTable;
        }
        public static bool InsertData(string sql)
        {
            return Transaction(sql);
        }

        public static bool UpdateData(string sql)
        {
            return Transaction(sql);
        }

        private static bool Transaction(string sql)
        {
            using (SqlCommand cmd = new SqlCommand(sql, connection))
            {
                Open();
                cmd.ExecuteNonQuery();
                Close();
                return true;
            }
            throw new Exception();
        }
        public static double GetScalar(string sql)
        {
            Open();
            using (var cmd = new SqlCommand(sql, connection))
            {
                
                double cost =(double) cmd.ExecuteScalar();
                Close();
                return cost;
            }
        }
        public static object GetScalarValue(string sql)
        {
            using(var cmd = new SqlCommand(sql, connection))
            {
                Open();
                var obj = cmd.ExecuteScalar();
                Close();
                return obj;
            }
        }

        #region Open and Close DB

        static void Open()
        {
            connection.Open();
        }
        static void Close()
        {
            connection.Close();
        }
        #endregion
    }
}
