﻿using SchoolMG.DAL;
using SchoolMG.Enumerator;
using System;
using System.Linq;
using System.Windows.Forms;
using SchoolMG.Model;

namespace SchoolMG.Register
{
    public partial class Payment : UserControl
    {
        public ChooseClass ChooseClass;
        ClassDal ClassDal;
        public ClassModel ClassModel;

        

        public Payment()
        {
            InitializeComponent();

            ClassDal = new ClassDal();
            ClassModel = new ClassModel();


        }

        void GetTotalCost()
        {
            if (string.IsNullOrWhiteSpace(txtDiscount.Text)) discount = 0;
            else
            {
                discount = Convert.ToDouble(txtDiscount.Text);
            }
            totalCost = courseCost - (courseCost * discount / 100);
            txtTotalCoursePrice.Text = $"{totalCost:#,##0.00}";
        }
        void GetOwed()
        {
            if (string.IsNullOrWhiteSpace(txtPaid.Text)) paid = 0;
            else paid = Convert.ToDouble(txtPaid.Text);

            owed = totalCost - paid;
            lblPaidDisplay.Text = paid.ToString("#,##0.00");
            txtOwed.Text = owed.ToString("#,##0.00");
        }

        private double courseCost {
            get
            {
                if (string.IsNullOrWhiteSpace(txtPrice.Text)) return 0;
                return Convert.ToInt32(txtPrice.Text);
            }
        }
        private double discount;
        public double paid;
        private double totalCost;
        private double owed;

        public double Paid
        {
            get
            {
                if (paid > totalCost)
                {
                    return totalCost;
                }
                return paid;
            }
        }

        public double TotalCost => totalCost;
        public double Cost { get => courseCost; }
        public double Owed => owed;
        public double Discount => discount / 100;
        public StudentStatus StudentStatus;

        private void txtDiscount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((string.IsNullOrWhiteSpace(txtDiscount.Text) && e.KeyChar == 46)

                || Checking.IsKhmerDigit(e.KeyChar))
            {
                e.Handled = true;
                return;
            }
            if (e.KeyChar == 46 && txtDiscount.Text.Contains("."))
            {
                e.Handled = true;
                return;
            }
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != 46)
            {
                e.Handled = true;
                return;
            }

            Console.WriteLine("CharCode: {0}", (int)e.KeyChar);
        }

        private void txtPaid_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtDiscount.Text) && e.KeyChar == 46
                 || Checking.IsKhmerDigit(e.KeyChar))
            {
                e.Handled = true;
                return;
            }
            if (e.KeyChar == 46 && txtDiscount.Text.Contains("."))
            {
                e.Handled = true;
                return;
            }
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != 46)
            {

                e.Handled = true;
                return;
            }
            Console.WriteLine("CharCode: {0}", (int)e.KeyChar);
        }

        private void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            GetTotalCost();
        }

        private void txtPaid_TextChanged(object sender, EventArgs e)
        {

            GetOwed();
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            
            GetTotalCost();
            
        }

        private void txtTotalCoursePrice_TextChanged(object sender, EventArgs e)
        {
           
            GetOwed();
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            
        }
    }
}
