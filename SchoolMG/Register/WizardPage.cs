﻿using System.Drawing;
using System.Windows.Forms;

namespace SchoolMG.Register
{
    public class WizardPage
    {
        public string Title { get; set; }
        public Image Image { get; set; }
        public UserControl Page { get; set; }
    }
}
