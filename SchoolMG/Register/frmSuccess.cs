﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Register
{
    public partial class frmSuccess : Form
    {
        int count = 0;
        public frmSuccess()
        {
            InitializeComponent();
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (count == 5)
            {
                timer.Stop();
                this.Close();
            }
            count++;
            Console.WriteLine(count);
        }
    }
}
