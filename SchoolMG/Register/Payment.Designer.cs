﻿
namespace SchoolMG.Register
{
    partial class Payment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNote = new System.Windows.Forms.Label();
            this.lblCost = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.txtTotalCoursePrice = new System.Windows.Forms.TextBox();
            this.lblTotalCost = new System.Windows.Forms.Label();
            this.lblPaid = new System.Windows.Forms.Label();
            this.lblOwed = new System.Windows.Forms.Label();
            this.txtPaid = new System.Windows.Forms.TextBox();
            this.txtOwed = new System.Windows.Forms.TextBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblPaidDisplay = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.26601F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.73399F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.lblNote, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblCost, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtPrice, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDiscount, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDiscount, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtTotalCoursePrice, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTotalCost, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblPaid, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblOwed, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtPaid, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtOwed, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtNote, 1, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(35, 46);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(462, 409);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(4, 340);
            this.lblNote.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNote.Name = "lblNote";
            this.lblNote.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblNote.Size = new System.Drawing.Size(82, 27);
            this.lblNote.TabIndex = 13;
            this.lblNote.Text = "កំណត់ត្រា";
            // 
            // lblCost
            // 
            this.lblCost.AutoSize = true;
            this.lblCost.Location = new System.Drawing.Point(4, 0);
            this.lblCost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCost.Name = "lblCost";
            this.lblCost.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblCost.Size = new System.Drawing.Size(47, 27);
            this.lblCost.TabIndex = 0;
            this.lblCost.Text = "តម្លៃ";
            // 
            // txtPrice
            // 
            this.txtPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrice.BackColor = System.Drawing.Color.White;
            this.txtPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrice.Location = new System.Drawing.Point(153, 6);
            this.txtPrice.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.ReadOnly = true;
            this.txtPrice.Size = new System.Drawing.Size(305, 35);
            this.txtPrice.TabIndex = 7;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.Location = new System.Drawing.Point(4, 68);
            this.lblDiscount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblDiscount.Size = new System.Drawing.Size(108, 27);
            this.lblDiscount.TabIndex = 0;
            this.lblDiscount.Text = "បញ្ចុះតម្លៃ(%)";
            // 
            // txtDiscount
            // 
            this.txtDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiscount.BackColor = System.Drawing.Color.White;
            this.txtDiscount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDiscount.Location = new System.Drawing.Point(153, 74);
            this.txtDiscount.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(305, 35);
            this.txtDiscount.TabIndex = 8;
            this.txtDiscount.TextChanged += new System.EventHandler(this.txtDiscount_TextChanged);
            this.txtDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscount_KeyPress);
            // 
            // txtTotalCoursePrice
            // 
            this.txtTotalCoursePrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalCoursePrice.BackColor = System.Drawing.Color.White;
            this.txtTotalCoursePrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalCoursePrice.Location = new System.Drawing.Point(153, 142);
            this.txtTotalCoursePrice.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtTotalCoursePrice.Name = "txtTotalCoursePrice";
            this.txtTotalCoursePrice.ReadOnly = true;
            this.txtTotalCoursePrice.Size = new System.Drawing.Size(305, 35);
            this.txtTotalCoursePrice.TabIndex = 9;
            this.txtTotalCoursePrice.TextChanged += new System.EventHandler(this.txtTotalCoursePrice_TextChanged);
            // 
            // lblTotalCost
            // 
            this.lblTotalCost.AutoSize = true;
            this.lblTotalCost.Location = new System.Drawing.Point(4, 136);
            this.lblTotalCost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalCost.Name = "lblTotalCost";
            this.lblTotalCost.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblTotalCost.Size = new System.Drawing.Size(67, 27);
            this.lblTotalCost.TabIndex = 0;
            this.lblTotalCost.Text = "ថ្លៃសរុប";
            // 
            // lblPaid
            // 
            this.lblPaid.AutoSize = true;
            this.lblPaid.Location = new System.Drawing.Point(4, 204);
            this.lblPaid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPaid.Name = "lblPaid";
            this.lblPaid.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblPaid.Size = new System.Drawing.Size(97, 27);
            this.lblPaid.TabIndex = 0;
            this.lblPaid.Text = "ប្រាក់បានបង់";
            // 
            // lblOwed
            // 
            this.lblOwed.AutoSize = true;
            this.lblOwed.Location = new System.Drawing.Point(4, 272);
            this.lblOwed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOwed.Name = "lblOwed";
            this.lblOwed.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblOwed.Size = new System.Drawing.Size(57, 27);
            this.lblOwed.TabIndex = 0;
            this.lblOwed.Text = "ជំពាក់";
            // 
            // txtPaid
            // 
            this.txtPaid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPaid.BackColor = System.Drawing.Color.White;
            this.txtPaid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaid.Location = new System.Drawing.Point(153, 210);
            this.txtPaid.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtPaid.Name = "txtPaid";
            this.txtPaid.Size = new System.Drawing.Size(305, 35);
            this.txtPaid.TabIndex = 10;
            this.txtPaid.TextChanged += new System.EventHandler(this.txtPaid_TextChanged);
            this.txtPaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPaid_KeyPress);
            // 
            // txtOwed
            // 
            this.txtOwed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOwed.BackColor = System.Drawing.Color.White;
            this.txtOwed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOwed.Location = new System.Drawing.Point(153, 278);
            this.txtOwed.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtOwed.Name = "txtOwed";
            this.txtOwed.ReadOnly = true;
            this.txtOwed.Size = new System.Drawing.Size(305, 35);
            this.txtOwed.TabIndex = 11;
            // 
            // txtNote
            // 
            this.txtNote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.txtNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNote.Location = new System.Drawing.Point(153, 346);
            this.txtNote.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(305, 57);
            this.txtNote.TabIndex = 12;
            // 
            // lblPaidDisplay
            // 
            this.lblPaidDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPaidDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(255)))));
            this.lblPaidDisplay.Font = new System.Drawing.Font("Khmer OS Siemreap", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaidDisplay.ForeColor = System.Drawing.Color.Blue;
            this.lblPaidDisplay.Location = new System.Drawing.Point(215, -3);
            this.lblPaidDisplay.Name = "lblPaidDisplay";
            this.lblPaidDisplay.Size = new System.Drawing.Size(318, 46);
            this.lblPaidDisplay.TabIndex = 12;
            this.lblPaidDisplay.Text = "label12";
            this.lblPaidDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Payment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lblPaidDisplay);
            this.Font = new System.Drawing.Font("Khmer OS Siemreap", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "Payment";
            this.Size = new System.Drawing.Size(533, 500);
            this.Load += new System.EventHandler(this.Payment_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.Label lblTotalCost;
        private System.Windows.Forms.Label lblPaid;
        private System.Windows.Forms.Label lblOwed;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.TextBox txtPrice;
        public System.Windows.Forms.TextBox txtDiscount;
        public System.Windows.Forms.TextBox txtTotalCoursePrice;
        public System.Windows.Forms.TextBox txtPaid;
        public System.Windows.Forms.TextBox txtOwed;
        private System.Windows.Forms.Label lblPaidDisplay;
        private System.Windows.Forms.Label lblNote;
        public System.Windows.Forms.TextBox txtNote;
    }
}
