﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolMG.Enumerator;
using SchoolMG.Model;

namespace SchoolMG.Register
{
    public partial class frmPrintInvoice : Form
    {
        public Register Register;
        public Payment Payment;
        public StudentModel StudentModel;

        public string StudentName;
        public string Gender;
        public DateTime Dob;
        public string Phone;
        public string Facebook;
        public string Telegram;
        public string Email;
        public string Address;

        public string CourseName;
        public string Turn;
        public string Time;
        public string Room;
        public string Teacher;

        public double Cost;
        public double Discount;
        public double TotalCost => Cost - Cost * Discount;
        public double Paid;

        public string UserName;

        public double Owed
        {
            get
            {
                if (Paid > TotalCost)
                    return 0;

                return TotalCost - Paid;
            }
        }

        public double Change
        {
            get
            {
                if (Paid < TotalCost) return 0;

                return Paid - TotalCost;
            }
        }


        public frmPrintInvoice(string studentName, string gender, DateTime dob, string phone, string facebook, string telegram, string email, string address, string courseName, string turn, string time, string room, string teacher, double cost, double discount, double paid, string userName)
        {
            InitializeComponent();

            StudentName = studentName;
            Gender = gender;
            Dob = dob;
            Phone = phone;
            Facebook = string.IsNullOrWhiteSpace(facebook)? "N/A": facebook;
            Telegram = string.IsNullOrWhiteSpace(telegram) ? "N/A" : telegram;
            Email = string.IsNullOrWhiteSpace(email) ? "N/A" : email;
            Address = string.IsNullOrWhiteSpace(address) ? "N/A" : address;

            CourseName = courseName;
            Turn = turn;
            Time = time;
            Room = room;
            Teacher = teacher;

            Cost = cost;
            Discount = discount;
            Paid = paid;
            UserName = userName;

        }
        //public frmPrintInvoice(Register register, Payment payment)
        //{
        //    InitializeComponent();
        //    Register = register;
        //    Payment = payment;
        //}

        private void frmPrintInvoice_Load(object sender, EventArgs e)
        {
            Microsoft.Reporting.WinForms.ReportParameter[] reportParameters = new Microsoft.Reporting.WinForms.ReportParameter[]
            {
                new Microsoft.Reporting.WinForms.ReportParameter("pStudentName", StudentName),
                new Microsoft.Reporting.WinForms.ReportParameter("pGender",Gender),
                new Microsoft.Reporting.WinForms.ReportParameter("pDoB", Dob.ToString("dd/MM/yyyy")),
                new Microsoft.Reporting.WinForms.ReportParameter("pPhoneNumber", Phone),
                new Microsoft.Reporting.WinForms.ReportParameter("pFacebook", Facebook),
                new Microsoft.Reporting.WinForms.ReportParameter("pTelegram", Telegram),
                new Microsoft.Reporting.WinForms.ReportParameter("pEmail", Email),
                new Microsoft.Reporting.WinForms.ReportParameter("pAddress", Address),
                
                new Microsoft.Reporting.WinForms.ReportParameter("pCourse", CourseName),
                new Microsoft.Reporting.WinForms.ReportParameter("pTurn", Turn),
                new Microsoft.Reporting.WinForms.ReportParameter("pTime", Time),
                new Microsoft.Reporting.WinForms.ReportParameter("pRoom", Room),
                new Microsoft.Reporting.WinForms.ReportParameter("pTeacher", Teacher),
                new Microsoft.Reporting.WinForms.ReportParameter("pCost", Cost.ToString("C")),
                new Microsoft.Reporting.WinForms.ReportParameter("pDiscount", Discount.ToString("P")),
                new Microsoft.Reporting.WinForms.ReportParameter("pTotalCost", TotalCost.ToString("C")),
                new Microsoft.Reporting.WinForms.ReportParameter("pPaid", Paid.ToString("C")),
                new Microsoft.Reporting.WinForms.ReportParameter("pOwed", Owed.ToString("C")),
                new Microsoft.Reporting.WinForms.ReportParameter("pChange", Change.ToString("C")),
                new Microsoft.Reporting.WinForms.ReportParameter("pCashier", UserName),
            };

            this.reportViewer1.LocalReport.SetParameters(reportParameters);
            this.reportViewer1.LocalReport.DisplayName = $"{StudentName} {DateTime.Now}" ;
            this.reportViewer1.RefreshReport();
        }
    }
}
