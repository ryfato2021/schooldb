﻿using SchoolMG.DAL;
using SchoolMG.Enumerator;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SchoolMG.Register
{
    public partial class Register : UserControl
    {
        string photoDir = @"Image\Student";
        string photoFileName = string.Empty;
        string RootPath = Path.GetFullPath(@"..\..");

        string srcFileName = string.Empty;

        public string PhotoFullPath
        {
            get
            {
                if (string.IsNullOrWhiteSpace(photoFileName))
                    return "";
                return Path.Combine(photoDir, photoFileName);
            }
        }

        public Register()
        {
            InitializeComponent();

            // Bind Enum Gender to cbGender
            cbGender.DataSource = Enum.GetValues(typeof(Gender));
        }

        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar)
                && !Checking.IsKhmerLetter(e.KeyChar)
                && e.KeyChar != 8
                && e.KeyChar != 32
                )
            {
                e.Handled = true;
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)
                && e.KeyChar != 8
                || Checking.IsKhmerDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtTelegram_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)
                && e.KeyChar != 8
                || Checking.IsKhmerDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Browse Portrait Photo";
            openFileDialog.Filter = "Image (*.PNG; *.JPG; *.GIF; *.TIFF)|*.PNG; *.JPG; *.GIF; *.TIFF";
            openFileDialog.FileName = string.Empty;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            try
            {
                StudentDal studentDal = new StudentDal();
                var studentId = studentDal.GetCurretStudentId() + 1;
                string studentName = txtName.Text;
                photoFileName = $"{studentId}-{studentName}";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    srcFileName = openFileDialog.FileName;
                    var extention = Path.GetExtension(srcFileName);
                    photoFileName += extention;

                    //MessageBox.Show(photoFileName);

                    picPortrait.Image = Image.FromFile(srcFileName);
                    picPortrait.SizeMode = PictureBoxSizeMode.Zoom;

                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex);
            }
        }
        public void UploadImage()
        {
            try
            {
                if (photoFileName != string.Empty)
                    File.Copy(srcFileName, Path.Combine(RootPath, photoDir, photoFileName), true);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex);
            }
            finally
            {
                photoFileName = string.Empty;
            }
        }
    }
}
