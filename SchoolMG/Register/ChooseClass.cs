﻿using SchoolMG.Addition;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SchoolMG.Register
{
    public partial class ChooseClass : UserControl
    {
        public Wizard Wizard;
        public Addition.ClassCard ClassCard;
        public DAL.ClassDal ClassDal;

        public ChooseClass()
        {
            InitializeComponent();
        }

        private void ChooseClass_Load(object sender, EventArgs e)
        {
            ClassDal = new DAL.ClassDal();


            DataTable tableTurn = new DataTable("Turn");
            tableTurn.Columns.Add("TurnId", typeof(int));
            tableTurn.Columns.Add("Turn", typeof(string));
            tableTurn.Rows.Add(0, "Weekday");
            tableTurn.Rows.Add(1, "Weekend");

            var row = tableTurn.NewRow();
            row["TurnId"] = -1;
            row["Turn"] = "- ជ្រើសរើសវេន -";
            tableTurn.Rows.InsertAt(row, 0);

            cbTurn.DataSource = tableTurn;
            cbTurn.DisplayMember = "Turn";
            cbTurn.ValueMember = "TurnId";

            var tableCourse = ClassDal.CourseDal.GetAllCourses();

            row = tableCourse.NewRow();
            row["Id"] = -1;
            row["Name"] = "- ជ្រើសរើសមុខវិជ្ជា -";
            tableCourse.Rows.InsertAt(row, 0);

            cbCourse.DataSource = tableCourse;
            cbCourse.DisplayMember = "Name";
            cbCourse.ValueMember = "Id";
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var courseId = cbCourse.SelectedValue;
            object turn = cbTurn.SelectedValue;
            if ((int)courseId == -1 || (int)turn == -1)
            {
                MessageBox.Show("Please Select Appropriate Course and Turn", "Informaion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if ((int)courseId == -1)
                    cbCourse.Focus();

                else cbTurn.Focus();

                return;
            }
            var table = ClassDal.GetClasses();
            var select = table.Select($"ClassStatus<>3 And (CourseId={courseId} And Turn={turn})");

            this.flowLayoutPanel.Controls.Clear();
            if (select.Count() > 0)
                foreach (var row in select)
                {
                    var classCard = getClassCard(row);
                    classCard.binding();

                    this.flowLayoutPanel.Controls.Add(classCard);
                    classCard.Click += ClassCard_Click;
                    classCard.pictureBox1.Click += PictureBox1_Click;
                }

            else
                this.flowLayoutPanel.Controls.Add(new Label
                {
                    Text = "មិនមានទិន្នន័យថ្នាក់ទេ",
                    AutoSize = true,

                });

            Console.WriteLine("Done");
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            var classCard = (ClassCard)((PictureBox)sender).Parent.Parent;
            Console.WriteLine("Class Is: " + classCard.CourseName);

            var price = Helper.DbHelper.GetScalar($"select distinct c.Cost from Classes Cl inner join Courses C On Cl.CourseId = C.Id where Cl.Id = {classCard.ClassId}");
            Payment payment = new Payment();
            Wizard.wizards[2].Page = payment;

            Wizard.ShowPage(Wizard.wizards[2]);
            Wizard.Stack.Push(Wizard.wizards[2]);

            Wizard.PaymentModel.ClassId = classCard.ClassId;
            Wizard.CourseName = classCard.CourseName;
            Wizard.Turn = classCard.Turn.ToString();
            Wizard.Time = classCard.Time;
            Wizard.Room = classCard.RoomName;
            Wizard.Teacher = classCard.TeacherName;
            Wizard.ClassStatus = classCard.ClassStatus;

            payment.txtPrice.Text = price.ToString();
        }

        private void ClassCard_Click(object sender, EventArgs e)
        {
            
        }

        private Addition.ClassCard getClassCard(DataRow row)
        {
            return new Addition.ClassCard()
            {
                Location = new System.Drawing.Point(0, 0),
                ClassId = row.Field<int>("ClassId"),
                CourseName = row.Field<string>("CourseName"),
                RoomName = row.Field<string>("RoomName"),
                Time = row.Field<string>("Time"),
                Turn = (Enumerator.Turn)row.Field<int>("Turn"),
                ClassStatus = (Enumerator.ClassState)row.Field<int>("ClassStatus"),
                TeacherName = row.Field<string>("TeacherName"),
                StudentAmount = row.Field<int>("CountStudent"),
                Picture = row.Field<string>("Picture")
            };
        }
    }
}
