﻿using SchoolMG.DAL;
using SchoolMG.Enumerator;
using SchoolMG.Helper;
using SchoolMG.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SchoolMG.Register
{
    public partial class Wizard : Form
    {
        public int UserId;
        public PaymentDal PaymentDal = new PaymentDal();
        public PaymentModel PaymentModel = new PaymentModel();
        public ClassModel classModel = new ClassModel();
        public ClassDal ClassDal = new ClassDal();
        public StudentModel StudentModel = new StudentModel();

        public StudentDal StudentDal;

        public Stack<WizardPage> Stack = new Stack<WizardPage>();

        public Payment Payment { get; set; }

        public bool IsOldStudent { get; set; }

        public List<WizardPage> wizards;
        int cursor;
        int count;

        public string CourseName;
        public double Cost;
        public string Turn;
        public string Time;
        public string Room;
        public string Teacher;
        public Enumerator.ClassState ClassStatus;


        public Wizard()
        {
            InitializeComponent();

            wizards = new List<WizardPage>()
            {
                new WizardPage()
                {
                    Title = "ចុះឈ្មោះសិស្សថ្មី",
                    Image = Image.FromFile(RootProject.ProjectPath + @"\Icons\student-registration_white.png"),
            Page = new Register()

                },
                new WizardPage()
                {
                    Title = "ជ្រើសរើសថ្នាក់ចូលរៀន",
                    Image =null,
            Page = new ChooseClass()
                },
                new WizardPage()
                {
                    Title = "បង់ប្រាក់",
                    Image = Image.FromFile(RootProject.ProjectPath + @"\Icons\payment_white.png"),
            Page = Payment
                }
            };

            count = wizards.Count;
        }

        // prepare for remove
        public void Next()
        {
            if (cursor < count - 1)
            {
                cursor++;
                ShowPage(wizards[1]);
                btnNext.Enabled = true;
            }
            if (cursor == count - 1)
            {
                btnNext.Enabled = false;
                btnBack.Enabled = true;
            }
        }

        public void Back()
        {
            WizardPage page = Stack.Pop();

            if (page.Page is Payment)
            {
                page = Stack.Peek();
                btnFinish.Visible = false;
            }
            else if (page.Page is ChooseClass)
            {
                page = Stack.Peek();
                btnNext.Visible = true;
                btnBack.Visible = false;
            }
            ShowPage(page);
            Console.WriteLine("Done");
        }

        public void ShowPage(WizardPage page)
        {

            lblHeader.Text = page.Title;
            picIcon.Image = page.Image;

            page.Page.Dock = DockStyle.Fill;
            pnlContent.Controls.Clear();
            pnlContent.Controls.Add(page.Page);

            if (page.Page is Register)
            {
                Stack.Clear();
                Stack.Push(page);

                btnBack.Visible = false;
                btnFinish.Visible = false;
                btnNext.Visible = true;
            }
            else if (page.Page is ChooseClass)
            {

                var choose = page.Page as ChooseClass;
                choose.Wizard = this;

                if (IsOldStudent) 
                    btnBack.Enabled = false;
                else
                    btnBack.Visible = true;

                btnNext.Visible = false;
                btnFinish.Visible = false;
            }
            else if (page.Page is Payment)
            {

                btnFinish.Visible = true;
                btnBack.Visible = true;
                btnNext.Visible = false;
            }

            //Stack.Push(page);

        }

        bool GetRegisterInput()
        {
            Register register = (Register)wizards[0].Page;

            bool isTextEmpty = CheckEmptyTextBoxs(register.txtName, register.txtPhone);

            if (isTextEmpty) return false;

            StudentModel.Id = StudentDal.GetCurretStudentId() + 1;
            StudentModel.Name = register.txtName.Text;
            Gender gender;
            Enum.TryParse<Gender>(register.cbGender.Text, out gender);
            StudentModel.Gender = (byte)gender;
            StudentModel.Dob = register.dtpDob.Value;
            StudentModel.Phone = register.txtPhone.Text;
            StudentModel.Facebook = register.txtFacebook.Text;
            StudentModel.Telegram = register.txtTelegram.Text;
            StudentModel.Email = register.txtEmail.Text;
            StudentModel.Address = register.txtAddress.Text;
            StudentModel.PhotoPath = register.PhotoFullPath;
            return true;
        }
        bool GetPaymentInput()
        {
            Payment payment = wizards[2].Page as Payment;

            if (payment is null) return false; ;

            PaymentModel.StudentId = StudentModel.Id;

            PaymentModel.StudentStatus = ClassStatus == ClassState.Active? (int)Enumerator.StudentStatus.Studying: (int)Enumerator.StudentStatus.Pendding;

            PaymentModel.Discount = payment.Discount;
            PaymentModel.PaidAmount = payment.Paid;
            PaymentModel.Note = payment.txtNote.Text;

            return true;
        }

        bool CheckEmptyTextBoxs(params TextBox[] textBoxes)
        {
            bool isEmpty = true;
            foreach (var text in textBoxes)
            {
                if (!string.IsNullOrWhiteSpace(text.Text))
                {
                    isEmpty = false;
                }
            }
            return isEmpty;
        }

        private void Wizard_Load(object sender, EventArgs e)
        {
            if (!IsOldStudent)
                ShowPage(wizards[0]);
            else ShowPage(wizards[1]);

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            var register = (Register)wizards[0].Page;
            bool isempty = CheckEmptyTextBoxs(register.txtName, register.txtPhone, register.txtFacebook, register.txtTelegram, register.txtEmail, register.txtAddress);

            if (isempty) this.Close();

            else if (MessageBox.Show("Are You Sure to Cancel??", "Cancel Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (GetRegisterInput() == false)
            {
                MessageBox.Show("Required Input Field Name or Phone Number", "Required Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ShowPage(wizards[1]);
            Stack.Push(wizards[1]);
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            GetPaymentInput();

            Print();

            if (!IsOldStudent)
            {
                var register = (Register)wizards[0].Page;
                register.UploadImage();

                StudentDal.InsertStudent(StudentModel.Name, StudentModel.Gender, StudentModel.Dob, StudentModel.Phone, StudentModel.Facebook, StudentModel.Telegram, StudentModel.Email, StudentModel.Address, StudentModel.PhotoPath);
            }


            PaymentDal.CreateStudentEnrollment(PaymentModel.StudentId, PaymentModel.ClassId, PaymentModel.StudentStatus, PaymentModel.Discount, PaymentModel.PaidAmount, PaymentModel.Note, UserId);


            if (IsOldStudent) this.Close();

            ClearChildTextBox();
            // Back
            ShowPage(wizards[0]);
        }

        bool Print()
        {
            Payment payment = wizards[2].Page as Payment;
            if (payment == null) return false;

            var cost = payment.Cost;
            var discount = payment.Discount;
            var paid = payment.paid;
            var totalCost = payment.TotalCost;
            var owed = payment.Owed;

            var userName = new UserDal().GetUsers().Rows.Find(UserId)["UserName"];
            Console.WriteLine();


            var gender = ((Enumerator.Gender)StudentModel.Gender).ToString();
            frmPrintInvoice frmPrintInvoice = new frmPrintInvoice(StudentModel.Name, gender, StudentModel.Dob, StudentModel.Phone, StudentModel.Facebook, StudentModel.Telegram, StudentModel.Email, StudentModel.Address, CourseName, Turn, Time, Room, Teacher, cost, discount, paid, userName.ToString());
            var rs = frmPrintInvoice.ShowDialog();
            Console.WriteLine(rs);
            return rs == DialogResult.Cancel;
        }
        void ClearChildTextBox()
        {
            wizards[0].Page = new Register();
            wizards[1].Page = new ChooseClass();
            wizards[2].Page = new Payment();
        }
    }
}
