﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Report
{
    public partial class StudentReportDetail : UserControl
    {
        public string CourseName { get; set; }
        public string Time { get; set; }
        public string Room { get; set; }
        public string Teacher { get; set; }

        public List<Model.StudentReportDetail> StudentReportDetails { get; set; }
        public StudentReportDetail()
        {
            InitializeComponent();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            DataTable data = (DataTable)studentReportDetailBindingSource.DataSource;
            List<Model.StudentReportDetail> list = new List<Model.StudentReportDetail>();
            foreach (DataRow row in data.Rows)
            {
                list.Add(new Model.StudentReportDetail
                {
                    StudentId = (int)row["StudentId"],
                    StudentName = row["StudentName"].ToString(),
                    Gender = row["Gender"].ToString(),
                    Phone = row["Phone"].ToString(),
                    EnrollDate = (DateTime)row["EnrollDate"],
                    StudentStatus = row["StudentStatus"].ToString(),
                    PaymentStatus = row["PaymentStatus"].ToString(),
                    Owed = (double)row["Owed"]
                });
            }
            var courseName = lblCourseNameShow.Text;
            var time = lblTimeShow.Text;
            var room = lblRoomShow.Text;
            var teacher = lblTeacherNameShow.Text;
            using (var report = new frmPrintStudentReport(courseName, time, room, teacher, list))
            {
                report.ShowDialog();
            }
            Console.WriteLine("Show");
        }

        private void StudentReportDetail_Load(object sender, EventArgs e)
        {

        }
    }
}
