﻿using SchoolMG.Addition;
using SchoolMG.Register;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Report
{
    public partial class StudentReport : UserControl
    {
        public Home Home;
        public DAL.ClassDal ClassDal = new DAL.ClassDal();
        public StudentReport()
        {
            InitializeComponent();

            var dtcourse = Helper.DbHelper.GetData("Select Id, Name From Courses");
            DataRow row = dtcourse.NewRow();
            row["Id"] = -1;
            row["Name"] = "-- Select Course --";
            dtcourse.Rows.InsertAt(row, 0);

            row = dtcourse.NewRow();
            row["Id"] = 0;
            row["Name"] = "All Courses";
            dtcourse.Rows.InsertAt(row, 1);

            cbCourse.DataSource = dtcourse;
            cbCourse.DisplayMember = "Name";
            cbCourse.ValueMember = "Id";
        }
        private Addition.ClassCard getClassCard(DataRow row)
        {
            return new Addition.ClassCard()
            {
                Location = new System.Drawing.Point(0, 0),
                ClassId = row.Field<int>("ClassId"),
                CourseName = row.Field<string>("CourseName"),
                RoomName = row.Field<string>("RoomName"),
                Time = row.Field<string>("Time"),
                Turn = (Enumerator.Turn)row.Field<int>("Turn"),
                ClassStatus = (Enumerator.ClassState)row.Field<int>("ClassStatus"),
                TeacherName = row.Field<string>("TeacherName"),
                StudentAmount = row.Field<int>("CountStudent"),
                Picture = row.Field<string>("Picture")
            };
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (cbCourse.SelectedIndex == 0)
            {
                MessageBox.Show("Select Course First", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            var tableClass = ClassDal.GetClasses();
            var view = new DataView(tableClass);
            view.Sort = "ClassId Desc";

            if (cbCourse.SelectedIndex > 1)
            {
                view.RowFilter = $"CourseName='{cbCourse.Text}'";
            }

            flowLayoutPanel.Controls.Clear();
            if (view.Count == 0)
            {

                flowLayoutPanel.Controls.Add(new Label()
                {
                    Text = "មិនមានទិន្នន័យថ្នាក់ទេ",
                    AutoSize = true,
                });
                return;
            }

            foreach(DataRow row in view.ToTable().Rows)
            {
                Addition.ClassCard classcard = getClassCard(row);
                classcard.binding();

                flowLayoutPanel.Controls.Add(classcard);
                classcard.pictureBox1.Click += PictureBox1_Click;
            }
            Console.WriteLine($"done: {view.Count}");
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            var classCard = (ClassCard)((PictureBox)sender).Parent.Parent;
            Console.WriteLine("Class Is: " + classCard.CourseName);

            StudentReportDetail studentReportDetail = new StudentReportDetail();
            studentReportDetail.lblCourseNameShow.Text = classCard.CourseName;
            studentReportDetail.lblTimeShow.Text = classCard.Time;
            studentReportDetail.lblRoomShow.Text = classCard.RoomName;
            studentReportDetail.lblTeacherNameShow.Text = classCard.TeacherName;

            var report = new DAL.ReportDal();
            var tableStudentReportDatail = report.GetStudentReportByClass(classCard.ClassId);
            studentReportDetail.studentReportDetailBindingSource.DataSource = tableStudentReportDatail;

            if (tableStudentReportDatail.Rows.Count <= 0)
            {
                studentReportDetail.panel.Controls.Clear();
                studentReportDetail.panel.Controls.Add(new Label
                {
                    Text = "មិនមានសិស្ស សិក្សាថ្នាក់នេះទេ",
                    AutoSize = false,
                    Dock = DockStyle.Fill,
                    Font = new Font("Khmer Os Battambang", 18),
                    ForeColor = Color.FromArgb(127, 127, 127, 127),
                    TextAlign = ContentAlignment.TopCenter
                }); ;

            }

            Home.ShowUp(studentReportDetail);
        }
    }
}
