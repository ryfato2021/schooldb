﻿
namespace SchoolMG.Report
{
    partial class StudentReportDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnPrint = new System.Windows.Forms.Button();
            this.dgvSellReport = new System.Windows.Forms.DataGridView();
            this.studentReportDetailBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblCourseName = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblTeacherName = new System.Windows.Forms.Label();
            this.lblRoom = new System.Windows.Forms.Label();
            this.lblTeacherNameShow = new System.Windows.Forms.Label();
            this.lblRoomShow = new System.Windows.Forms.Label();
            this.lblTimeShow = new System.Windows.Forms.Label();
            this.lblCourseNameShow = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.studentIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enrollDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.owedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSellReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentReportDetailBindingSource)).BeginInit();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(118)))), ((int)(((byte)(210)))));
            this.btnPrint.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPrint.Location = new System.Drawing.Point(1198, 115);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(130, 35);
            this.btnPrint.TabIndex = 28;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // dgvSellReport
            // 
            this.dgvSellReport.AllowUserToAddRows = false;
            this.dgvSellReport.AllowUserToDeleteRows = false;
            this.dgvSellReport.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgvSellReport.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSellReport.AutoGenerateColumns = false;
            this.dgvSellReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvSellReport.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvSellReport.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.dgvSellReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSellReport.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvSellReport.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dgvSellReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(41)))), ((int)(((byte)(72)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS Battambang", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(10);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(65)))), ((int)(((byte)(81)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSellReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvSellReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSellReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.studentIdDataGridViewTextBoxColumn,
            this.studentNameDataGridViewTextBoxColumn,
            this.genderDataGridViewTextBoxColumn,
            this.phoneDataGridViewTextBoxColumn,
            this.enrollDateDataGridViewTextBoxColumn,
            this.studentStatusDataGridViewTextBoxColumn,
            this.paymentStatusDataGridViewTextBoxColumn,
            this.owedDataGridViewTextBoxColumn});
            this.dgvSellReport.DataSource = this.studentReportDetailBindingSource;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Khmer OS Battambang", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSellReport.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvSellReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSellReport.EnableHeadersVisualStyles = false;
            this.dgvSellReport.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.dgvSellReport.Location = new System.Drawing.Point(0, 0);
            this.dgvSellReport.Name = "dgvSellReport";
            this.dgvSellReport.ReadOnly = true;
            this.dgvSellReport.RowHeadersVisible = false;
            this.dgvSellReport.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle7.Padding = new System.Windows.Forms.Padding(5, 10, 5, 10);
            this.dgvSellReport.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvSellReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSellReport.Size = new System.Drawing.Size(1311, 439);
            this.dgvSellReport.TabIndex = 21;
            // 
            // studentReportDetailBindingSource
            // 
            this.studentReportDetailBindingSource.DataSource = typeof(SchoolMG.Model.StudentReportDetail);
            // 
            // lblCourseName
            // 
            this.lblCourseName.AutoSize = true;
            this.lblCourseName.Location = new System.Drawing.Point(23, 35);
            this.lblCourseName.Name = "lblCourseName";
            this.lblCourseName.Size = new System.Drawing.Size(52, 27);
            this.lblCourseName.TabIndex = 29;
            this.lblCourseName.Text = "មុខវិជ្ជា";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(23, 62);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(42, 27);
            this.lblTime.TabIndex = 30;
            this.lblTime.Text = "ម៉ោង";
            // 
            // lblTeacherName
            // 
            this.lblTeacherName.AutoSize = true;
            this.lblTeacherName.Location = new System.Drawing.Point(23, 116);
            this.lblTeacherName.Name = "lblTeacherName";
            this.lblTeacherName.Size = new System.Drawing.Size(72, 27);
            this.lblTeacherName.TabIndex = 32;
            this.lblTeacherName.Text = "គ្រូបង្រៀន";
            // 
            // lblRoom
            // 
            this.lblRoom.AutoSize = true;
            this.lblRoom.Location = new System.Drawing.Point(23, 89);
            this.lblRoom.Name = "lblRoom";
            this.lblRoom.Size = new System.Drawing.Size(42, 27);
            this.lblRoom.TabIndex = 31;
            this.lblRoom.Text = "បន្ទប់";
            // 
            // lblTeacherNameShow
            // 
            this.lblTeacherNameShow.AutoSize = true;
            this.lblTeacherNameShow.Location = new System.Drawing.Point(132, 116);
            this.lblTeacherNameShow.Name = "lblTeacherNameShow";
            this.lblTeacherNameShow.Size = new System.Drawing.Size(109, 27);
            this.lblTeacherNameShow.TabIndex = 36;
            this.lblTeacherNameShow.Text = "Teacher Name";
            // 
            // lblRoomShow
            // 
            this.lblRoomShow.AutoSize = true;
            this.lblRoomShow.Location = new System.Drawing.Point(132, 89);
            this.lblRoomShow.Name = "lblRoomShow";
            this.lblRoomShow.Size = new System.Drawing.Size(51, 27);
            this.lblRoomShow.TabIndex = 35;
            this.lblRoomShow.Text = "Room";
            // 
            // lblTimeShow
            // 
            this.lblTimeShow.AutoSize = true;
            this.lblTimeShow.Location = new System.Drawing.Point(132, 62);
            this.lblTimeShow.Name = "lblTimeShow";
            this.lblTimeShow.Size = new System.Drawing.Size(44, 27);
            this.lblTimeShow.TabIndex = 34;
            this.lblTimeShow.Text = "Time";
            // 
            // lblCourseNameShow
            // 
            this.lblCourseNameShow.AutoSize = true;
            this.lblCourseNameShow.Location = new System.Drawing.Point(132, 35);
            this.lblCourseNameShow.Name = "lblCourseNameShow";
            this.lblCourseNameShow.Size = new System.Drawing.Size(103, 27);
            this.lblCourseNameShow.TabIndex = 33;
            this.lblCourseNameShow.Text = "Course Name";
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.Controls.Add(this.dgvSellReport);
            this.panel.Location = new System.Drawing.Point(17, 156);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1311, 439);
            this.panel.TabIndex = 37;
            // 
            // studentIdDataGridViewTextBoxColumn
            // 
            this.studentIdDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.studentIdDataGridViewTextBoxColumn.DataPropertyName = "StudentId";
            dataGridViewCellStyle3.Format = "00000";
            this.studentIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.studentIdDataGridViewTextBoxColumn.HeaderText = "អត្តលេខ";
            this.studentIdDataGridViewTextBoxColumn.Name = "studentIdDataGridViewTextBoxColumn";
            this.studentIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.studentIdDataGridViewTextBoxColumn.Width = 105;
            // 
            // studentNameDataGridViewTextBoxColumn
            // 
            this.studentNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.studentNameDataGridViewTextBoxColumn.DataPropertyName = "StudentName";
            this.studentNameDataGridViewTextBoxColumn.HeaderText = "ឈ្មោះ";
            this.studentNameDataGridViewTextBoxColumn.Name = "studentNameDataGridViewTextBoxColumn";
            this.studentNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // genderDataGridViewTextBoxColumn
            // 
            this.genderDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.genderDataGridViewTextBoxColumn.DataPropertyName = "Gender";
            this.genderDataGridViewTextBoxColumn.HeaderText = "ភេទ";
            this.genderDataGridViewTextBoxColumn.Name = "genderDataGridViewTextBoxColumn";
            this.genderDataGridViewTextBoxColumn.ReadOnly = true;
            this.genderDataGridViewTextBoxColumn.Width = 80;
            // 
            // phoneDataGridViewTextBoxColumn
            // 
            this.phoneDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.phoneDataGridViewTextBoxColumn.DataPropertyName = "Phone";
            this.phoneDataGridViewTextBoxColumn.HeaderText = "លេខទូរសព្ឬ";
            this.phoneDataGridViewTextBoxColumn.Name = "phoneDataGridViewTextBoxColumn";
            this.phoneDataGridViewTextBoxColumn.ReadOnly = true;
            this.phoneDataGridViewTextBoxColumn.Width = 125;
            // 
            // enrollDateDataGridViewTextBoxColumn
            // 
            this.enrollDateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.enrollDateDataGridViewTextBoxColumn.DataPropertyName = "EnrollDate";
            dataGridViewCellStyle4.Format = "dd/MM/yyyy";
            this.enrollDateDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.enrollDateDataGridViewTextBoxColumn.HeaderText = "ថ្ងៃចុះឈ្មោះ";
            this.enrollDateDataGridViewTextBoxColumn.Name = "enrollDateDataGridViewTextBoxColumn";
            this.enrollDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.enrollDateDataGridViewTextBoxColumn.Width = 125;
            // 
            // studentStatusDataGridViewTextBoxColumn
            // 
            this.studentStatusDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.studentStatusDataGridViewTextBoxColumn.DataPropertyName = "StudentStatus";
            this.studentStatusDataGridViewTextBoxColumn.HeaderText = "ស្ថានភាពសិស្ស";
            this.studentStatusDataGridViewTextBoxColumn.Name = "studentStatusDataGridViewTextBoxColumn";
            this.studentStatusDataGridViewTextBoxColumn.ReadOnly = true;
            this.studentStatusDataGridViewTextBoxColumn.Width = 146;
            // 
            // paymentStatusDataGridViewTextBoxColumn
            // 
            this.paymentStatusDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.paymentStatusDataGridViewTextBoxColumn.DataPropertyName = "PaymentStatus";
            this.paymentStatusDataGridViewTextBoxColumn.HeaderText = "ស្ថានភាពបង់ប្រាក់";
            this.paymentStatusDataGridViewTextBoxColumn.Name = "paymentStatusDataGridViewTextBoxColumn";
            this.paymentStatusDataGridViewTextBoxColumn.ReadOnly = true;
            this.paymentStatusDataGridViewTextBoxColumn.Width = 161;
            // 
            // owedDataGridViewTextBoxColumn
            // 
            this.owedDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.owedDataGridViewTextBoxColumn.DataPropertyName = "Owed";
            dataGridViewCellStyle5.Format = "N2";
            this.owedDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.owedDataGridViewTextBoxColumn.HeaderText = "ជំពាក់";
            this.owedDataGridViewTextBoxColumn.Name = "owedDataGridViewTextBoxColumn";
            this.owedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // StudentReportDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel);
            this.Controls.Add(this.lblTeacherNameShow);
            this.Controls.Add(this.lblRoomShow);
            this.Controls.Add(this.lblTimeShow);
            this.Controls.Add(this.lblCourseNameShow);
            this.Controls.Add(this.lblTeacherName);
            this.Controls.Add(this.lblRoom);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblCourseName);
            this.Controls.Add(this.btnPrint);
            this.Font = new System.Drawing.Font("Khmer OS Battambang", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StudentReportDetail";
            this.Size = new System.Drawing.Size(1350, 615);
            this.Load += new System.EventHandler(this.StudentReportDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSellReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentReportDetailBindingSource)).EndInit();
            this.panel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrint;
        public System.Windows.Forms.DataGridView dgvSellReport;
        private System.Windows.Forms.Label lblCourseName;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblTeacherName;
        private System.Windows.Forms.Label lblRoom;
        public System.Windows.Forms.Label lblTeacherNameShow;
        public System.Windows.Forms.Label lblRoomShow;
        public System.Windows.Forms.Label lblTimeShow;
        public System.Windows.Forms.Label lblCourseNameShow;
        public System.Windows.Forms.Panel panel;
        public System.Windows.Forms.BindingSource studentReportDetailBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn genderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn enrollDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn owedDataGridViewTextBoxColumn;
    }
}
