﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace SchoolMG.Report
{
    public partial class frmPrintStudentReport : Form
    {
        public frmPrintStudentReport(string courseName, string time, string room, string teacher, List<Model.StudentReportDetail> studentReport)
        {
            InitializeComponent();

            CourseName = courseName;
            Time = time;
            Room = room;
            Teacher = teacher;
            StudentReport = studentReport;
            
        }

        public string CourseName { get; set; }
        public string Time { get; set; }
        public string Room { get; set; }
        public string Teacher { get; set; }
        public List<Model.StudentReportDetail> StudentReport { get; set; }

        private void frmPrintStudentReport_Load(object sender, EventArgs e)
        {
            ReportParameter[] reportParameters = new ReportParameter[]
            {
                new ReportParameter("pCourseName", CourseName),
                new ReportParameter("pTime", Time),
                new ReportParameter("pRoom", Room),
                new ReportParameter("pTeacherName", Teacher)
            };
            this.reportViewer1.LocalReport.SetParameters(reportParameters);
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", StudentReport));

            var Date = DateTime.Now;
            string today = string.Format("{0:ddMMyyyy}-{1}h{2}m{3}s",Date, Date.Hour, Date.Minute, Date.Second);
            this.reportViewer1.LocalReport.DisplayName = $"{CourseName}-{Time}-{Room}-{Teacher}-{today}";
            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
        }
    }
}
