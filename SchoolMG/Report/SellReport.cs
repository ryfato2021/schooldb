﻿using SchoolMG.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SchoolMG.Report
{
    public partial class SellReport : UserControl
    {
        public Home Home;
        public ReportDal Report;
        public SellReport()
        {
            InitializeComponent();

            Report = new ReportDal();

            var dtcourse = Helper.DbHelper.GetData("Select Id, Name From Courses");
            DataRow row = dtcourse.NewRow();
            row["Id"] = -1;
            row["Name"] = "-- Select Course --";
            dtcourse.Rows.InsertAt(row, 0);

            row = dtcourse.NewRow();
            row["Id"] = 0;
            row["Name"] = "All Courses";
            dtcourse.Rows.InsertAt(row, 1);

            cbCourse.DataSource = dtcourse;
            cbCourse.DisplayMember = "Name";
            cbCourse.ValueMember = "Id";

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            var fromDate = dtpFromDate.Value;
            var toDate = dtpToDate.Value;
            var courseName = cbCourse.Text;

            var expression = string.Empty;
            
            var table = Report.GetSellReportByDate(fromDate, toDate);


            if ((int)cbCourse.SelectedValue == -1)
            {
                cbCourse.Focus();
                MessageBox.Show("Select Course First");
                return;
            }
            if ((int)cbCourse.SelectedValue != 0)
            {
                expression += $"CourseName = '{courseName}'";
            }

            sellReportBindingSource.DataSource = table;
            sellReportBindingSource.Filter = expression;

            

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            List<Model.SellReport> sells = new List<Model.SellReport>();

            dgvSellReport.DataSource = sellReportBindingSource;
            var da = sellReportBindingSource.List;

            foreach (DataRowView view in da)
            {
                Model.SellReport sell = new Model.SellReport()
                {
                    Id = (int)view["Id"],
                    StudentId = (int)view["StudentId"],
                    StudentName = view["StudentName"].ToString(),
                    Gender = view["Gender"].ToString(),
                    Phone = view["Phone"].ToString(),
                    CourseName = view["CourseName"].ToString(),
                    PaidAmount = (double)view["PaidAmount"],
                    Owed = (double)view["Owed"],
                    PaymentStatus = view["PaymentStatus"].ToString(),
                    PaidDate = (DateTime)view["PaidDate"],
                    Cashier = view["Cashier"].ToString()
                };

                sells.Add(sell);
            }

            frmPrintReport report = new frmPrintReport(sells);
            report.ShowDialog();
        }
    }
}
