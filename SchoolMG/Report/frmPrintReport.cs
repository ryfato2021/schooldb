﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Report
{
    public partial class frmPrintReport : Form
    {
        public List<Model.SellReport> sellReports;
        public frmPrintReport(List<Model.SellReport> sellReports)
        {
            this.sellReports = sellReports;
            
            InitializeComponent();
        }

        private void frmPrintReport_Load(object sender, EventArgs e)
        {
            SellReportBindingSource.DataSource = sellReports;

            var Date = DateTime.Now;
            string today = string.Format("{0:ddMMyyyy}-{1}h{2}m{3}s", Date, Date.Hour, Date.Minute, Date.Second);
            this.reportViewer1.LocalReport.DisplayName = $"SellReport-{today}";
            reportViewer1.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout);
            reportViewer1.BackColor = Color.FromArgb(244, 244, 244);
            this.reportViewer1.RefreshReport();
        }
    }
}
