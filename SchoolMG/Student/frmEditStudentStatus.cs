﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Student
{
    public partial class frmEditStudentStatus : Form
    {
        int output = -1;

        public string CurrentStatus { get; set; }

        public frmEditStudentStatus()
        {
            InitializeComponent();
            cbStudentStatus.DataSource = Enum.GetValues(typeof(SchoolMG.Enumerator.StudentStatus));
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public int ShowEditMode()
        {
            cbStudentStatus.Text = CurrentStatus;
            this.ShowDialog();
            return output;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(cbStudentStatus.Text!=CurrentStatus)
                output = (int)Enum.Parse(typeof(SchoolMG.Enumerator.StudentStatus), cbStudentStatus.Text);
            this.Close();
        }
    }
}
