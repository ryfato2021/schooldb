﻿using SchoolMG.DAL;
using SchoolMG.Register;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SchoolMG.Student
{
    public partial class StudentInfo : UserControl
    {
        public StudentDal studentDal = new StudentDal();
        public StudentDetail StudentDetail;
        public DataTable dataTable = new DataTable();
        
        public StudentInfo()
        {
            InitializeComponent();
            
            PopulateStudent();
        }

        public void PopulateStudent()
        {
            dataTable = studentDal.GetAllStudents();
            dgvStudentInfo.DataSource = dataTable;

            dgvStudentInfo.DataSource = studentDal.GetAllStudents();

            dgvStudentInfo.Columns["Photo"].Visible = false;
            dgvStudentInfo.Columns["CreatedAt"].Visible = false;
            dgvStudentInfo.Columns["ModifiedAt"].Visible = false;

            DataGridViewImageColumn dataGridViewImageColumn = new DataGridViewImageColumn()
            {
                Name = "Details",
                HeaderText = "Details",
                Image = SchoolMG.Resource.info_30px_Blue,
                //DividerWidth = 200,
                Width = 75,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None
            };

            if (dgvStudentInfo.Columns.Contains(dgvStudentInfo.Columns["Details"]) ||
                dgvStudentInfo.Columns.Contains(dgvStudentInfo.Columns["Details"])) return;

            dgvStudentInfo.Columns.Add(dataGridViewImageColumn);
            dgvStudentInfo.Columns.Add("FreeColumn", "");
            dgvStudentInfo.Columns["FreeColumn"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

       
        private void StudentInfo_Load(object sender, EventArgs e)
        {

        }

        private void dgvStudentInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //var current = dgvStudentInfo.CurrentCell;
            //if (current is DataGridViewImageCell)
            //{
            //    var cell = dgvStudentInfo.SelectedCells[1];
            //    MessageBox.Show(cell.Value.ToString());
            //    var studentId = cell.Value;

            //    StudentDetail studentDetail;
            //    var selectedRow = dataTable.Select($"Id = {studentId}");
            //    if (selectedRow.Length > 0)
            //    {
            //        studentDetail = new StudentDetail();
            //    }
            //}

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var tb = studentDal.GetAllStudents();
            //var table = classDal.tableDisplay;

            //EnrollmentDal enrollmentDal = new EnrollmentDal();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            string search = txtSearch.Text;
            if (string.IsNullOrWhiteSpace(search))
            {
                txtSearch.Focus();
                return;
            }
            string Expression = $"Convert(Id, System.String)= '{search}' or Name like '%{search}%' or Facebook like '%{search}%' or Phone like '%{search}%'";
            var searchData = studentDal.GetAllStudents().Select(Expression);

            DataTable tableSearch = studentDal.GetAllStudents().Clone();
            if (searchData.Count() > 0)
            {
                dgvStudentInfo.DataSource = searchData.CopyToDataTable();
                return;
            }

            dgvStudentInfo.DataSource = tableSearch;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var RegisterWizard = new Wizard();
            RegisterWizard.StudentDal = studentDal;
            RegisterWizard.ShowDialog();
        }
    }
}
