﻿using SchoolMG.DAL;
using SchoolMG.Enumerator;
using SchoolMG.Helper;
using SchoolMG.Model;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SchoolMG.Student
{
    public partial class StudentDetail : UserControl
    {
        ClassDal classDal;
        EnrollmentDal EnrollmentDal;
        public StudentDal StudentDal;
        StudentModel StudentModel;
        DataRow RowStudentInfo;

        DataTable tableEnrollment;
        DataTable DisplayTable;

        public int StudentId { get; set; }

        public StudentDetail()
        {
            InitializeComponent();

            StudentModel = new StudentModel();
            EnrollmentDal = new EnrollmentDal();

            //SetTextBoxReadOnly();
            //SetChangeControl(isEditable);

            DisplayTable = new DataTable();

            DisplayTable.Columns.Add("Id");
            DisplayTable.Columns.Add("Course");
            DisplayTable.Columns.Add("Turn");
            DisplayTable.Columns.Add("Time");
            DisplayTable.Columns.Add("Room");
            DisplayTable.Columns.Add("Teacher");
            DisplayTable.Columns.Add("StudentStatus");

            DisplayTable.PrimaryKey = new DataColumn[] { DisplayTable.Columns["Id"] };




            this.Paint += StudentDetail_Paint;
        }
        int time;

        public void LoadStudentInfo()
        {
            tableEnrollment = EnrollmentDal.GetEnrollmentByStudent(StudentId);

            DisplayTable.Clear();
            foreach (DataRow row in tableEnrollment.Rows)
            {
                var rowClass = row.GetParentRow("ClassEnrollmentRelation");
                var rowCourse = rowClass.GetParentRow("CourseClassRelation");
                var rowTurn = rowClass.GetParentRow("TurnClassRelation");
                var rowTime = rowClass.GetParentRow("TimeClassRelation");
                var rowRoom = rowClass.GetParentRow("RoomClassRelation");
                var rowTeacher = rowClass.GetParentRow("TeacherClassRelation");

                DataRow newRow = DisplayTable.NewRow();
                newRow[0] = row["Id"];
                newRow[1] = rowCourse["Name"];
                newRow[2] = rowTurn["Turn"];
                newRow[3] = rowTime["FullTime"];
                newRow[4] = rowRoom["Name"];
                newRow[5] = rowTeacher["Name"];
                newRow[6] = (StudentStatus)row["StudentStatus"];

                DisplayTable.Rows.Add(newRow);
            }
            dgvStudyInfo.DataSource = DisplayTable;


           
        }
        public void ShowStudentStudayInfo()
        {
            LoadStudentInfo();

            var tableUser = new DAL.UserDal().GetUsers();
            tableEnrollment.DataSet.Tables.Add(tableUser);
            DataRelation enrollmentUserRelation = new DataRelation("EnrollmentUserRelation", tableUser.Columns["Id"], tableEnrollment.Columns["UserId"]);
            tableEnrollment.DataSet.Relations.Add(enrollmentUserRelation);

            dgvStudyInfo.Columns.Add(new DataGridViewImageColumn()
            {
                Name = "Edit",
                HeaderText = "Edit",
                Image = Resource.edit_20x20,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader

            });
            dgvStudyInfo.Columns.Add(new DataGridViewImageColumn()
            {
                Name = "Invoice",
                HeaderText = "Invoice",
                Image = Resource.icons8_receipt_32,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader

            });

            dgvStudyInfo.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "Empty",
                HeaderText = "",
            });

            dgvStudyInfo.Columns["Id"].Visible = false;


            tableEnrollment.TableNewRow += TableEnrollment_TableNewRow;
            tableEnrollment.RowChanged += TableEnrollment_RowChanged;
        }

        int count;
        private void TableEnrollment_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            Console.WriteLine(e.Action + " Row: " + e.Row.ItemArray);
            var row = DisplayTable.Rows.Find(e.Row["Id"]);
            if (row == null) return;
            row["StudentStatus"] = (StudentStatus)e.Row["StudentStatus"];
            Console.WriteLine("Count: {0}", ++count);
        }

        private void TableEnrollment_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void ShowStudentDetails(object studentId)
        {
            try
            {
                var rows = StudentDal.GetAllStudents().Select($"Id = {studentId}");

                if (rows.Length == 0)
                {
                    return;
                }

                RowStudentInfo = rows[0];

                if (RowStudentInfo != null)
                {
                    StudentModel.Id = (int)RowStudentInfo["Id"];
                    StudentModel.Name = RowStudentInfo["Name"].ToString();
                    StudentModel.Gender = (byte)RowStudentInfo["Gender"];
                    StudentModel.Dob = (DateTime)RowStudentInfo["DOB"];
                    StudentModel.Phone = RowStudentInfo["Phone"].ToString();
                    StudentModel.Facebook = RowStudentInfo["Facebook"].ToString();
                    StudentModel.Telegram = RowStudentInfo["Telegram"].ToString();
                    StudentModel.Email = RowStudentInfo["Email"].ToString();
                    StudentModel.Address = RowStudentInfo["Address"].ToString();
                    StudentModel.PhotoPath = RowStudentInfo["Photo"].ToString();
                }

                BindingDisplay();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void StudentDetail_Paint(object sender, PaintEventArgs e)
        {

        }

        void BindingDisplay()
        {
            txtName.Text = StudentModel.Name;
            txtGender.Text = ((Gender)StudentModel.Gender).ToString();
            txtDob.Text = StudentModel.Dob.ToString("dd-MM-yyyy");
            txtPhone.Text = StudentModel.Phone;
            txtFacebook.Text = StudentModel.Facebook;
            txtTelegram.Text = StudentModel.Telegram;
            txtEmail.Text = StudentModel.Email;
            txtAddress.Text = StudentModel.Address;

            string photoPath = Path.Combine(RootProject.ProjectPath, StudentModel.PhotoPath);
            if (File.Exists(photoPath))
            {
                picPortrait.Image = Image.FromFile(photoPath);
                picPortrait.SizeMode = PictureBoxSizeMode.Zoom;
            }
            else
            {
                picPortrait.Image = Resource.photos_48px;
                picPortrait.SizeMode = PictureBoxSizeMode.CenterImage;
            }

            tmpdob = StudentModel.Dob;
            tmpgender = StudentModel.Gender;

        }

        bool isReadOnly = true;
        DateTime tmpdob;
        byte tmpgender;
        ComboBox cbGender = new ComboBox();
        DateTimePicker dtpDob = new DateTimePicker();

        bool isReplaceImage;
        bool isEditMode;

        private void btnEditInfo_Click(object sender, EventArgs e)
        {

            SetChangeControl();

            SetTextBoxReadOnly();
            SetVisibleButton();

            SetEditMode();

            //isEditMode = !isEditMode;
        }
        void SetEditMode()
        {
            if (isEditMode)
            {
                btnEditInfo.Text = "Edit";
                BackUp();
                isEditMode = false;
            }
            else
            {
                btnEditInfo.Text = "Cancel";
                isEditMode = true;
            }
        }

        void SetChangeControl()
        {
            isReadOnly = !isReadOnly;

            if (!isReadOnly)
            {
                dtpDob.Value = tmpdob;
                dtpDob.Anchor = AnchorStyles.Left | AnchorStyles.Right;
                tableLayoutPanel1.Controls.Remove(txtDob);
                tableLayoutPanel1.Controls.Add(dtpDob, 1, 2);

                cbGender.DataSource = Enum.GetValues(typeof(Gender));
                cbGender.DropDownStyle = ComboBoxStyle.DropDownList;
                tableLayoutPanel1.Controls.Remove(txtGender);
                tableLayoutPanel1.Controls.Add(cbGender, 1, 1);
                cbGender.Text = ((Gender)tmpgender).ToString();

            }
            else
            {
                tmpdob = dtpDob.Value;
                txtDob.Text = tmpdob.ToString("dd-MM-yyyy");
                tableLayoutPanel1.Controls.Remove(dtpDob);
                tableLayoutPanel1.Controls.Add(txtDob, 1, 2);

                Gender gender;
                Enum.TryParse<Gender>(cbGender.Text, out gender);
                tmpgender = (byte)gender;
                txtGender.Text = gender.ToString();
                tableLayoutPanel1.Controls.Remove(cbGender);
                tableLayoutPanel1.Controls.Add(txtGender, 1, 1);
            }
        }
        void SetTextBoxReadOnly()
        {
            var isReadOnly = !txtName.ReadOnly;
            txtName.ReadOnly = isReadOnly;
            txtPhone.ReadOnly = isReadOnly;
            txtFacebook.ReadOnly = isReadOnly;
            txtTelegram.ReadOnly = isReadOnly;
            txtEmail.ReadOnly = isReadOnly;
            txtAddress.ReadOnly = isReadOnly;
            txtName.Focus();
        }
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        void SetVisibleButton()
        {
            btnBrowse.Visible = !btnBrowse.Visible;
            btnSave.Visible = !btnSave.Visible;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            SetVisibleButton();
            SetTextBoxReadOnly();

            SetChangeControl();

            BackUp();
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            Gender gender = (Gender)StudentModel.Gender;
            Enum.TryParse<Gender>(cbGender.Text, out gender);

            if (photoFileName != string.Empty)
                StudentModel.PhotoPath = Path.Combine(photoDir, photoFileName);

            StudentDal.UpdateStudent(txtName.Text, gender, dtpDob.Value, txtPhone.Text, txtFacebook.Text, txtTelegram.Text, txtEmail.Text, txtAddress.Text, StudentModel.PhotoPath, StudentModel.Id);

            UploadImage();


            SetChangeControl();
            SetTextBoxReadOnly();
            SetVisibleButton();

            btnEditInfo.Text = "Edit";

            MessageBox.Show("Done", "Expression Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        string photoDir = @"Image\Student";
        string photoFileName = string.Empty;
        string srcFileName = string.Empty;
        private void btnBrowse_Click(object sender, EventArgs e)
        {

            openFileDialog.Title = "Browse Portrait Photo";
            openFileDialog.Filter = "Image (*.PNG; *.JPG; *.GIF; *.TIFF)|*.PNG; *.JPG; *.GIF; *.TIFF";
            openFileDialog.FileName = string.Empty;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            try
            {
                var studentId = StudentModel.Id;
                string studentName = txtName.Text;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    photoFileName = $"{studentId}-{studentName}";

                    srcFileName = openFileDialog.FileName;
                    var extention = Path.GetExtension(srcFileName);
                    photoFileName += extention;

                    // Close Current Image, Otherwise it is not allowed to Replace Image
                    picPortrait.Image.Dispose();

                    picPortrait.Image = Image.FromFile(srcFileName);
                    picPortrait.SizeMode = PictureBoxSizeMode.Zoom;
                    isReplaceImage = true;
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex);
            }
        }
        void UploadImage()
        {
            try
            {
                if (!isReplaceImage) return;

                // Close Current Image, Otherwise it is not allowed to Replace Image

                File.Copy(srcFileName, Path.Combine(RootProject.ProjectPath, photoDir, photoFileName), true);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            photoFileName = string.Empty;
            isReplaceImage = false;
        }
        void BackUp()
        {
            txtName.Text = StudentModel.Name;
            txtGender.Text = ((Gender)StudentModel.Gender).ToString();
            txtDob.Text = StudentModel.Dob.ToString("dd-MM-yyyy");
            txtFacebook.Text = StudentModel.Facebook;
            txtTelegram.Text = StudentModel.Telegram;
            txtEmail.Text = StudentModel.Email;
            txtPhone.Text = StudentModel.Phone;
            txtAddress.Text = StudentModel.Address;

            if (StudentModel.PhotoPath != string.Empty)
            {
                picPortrait.Image = Image.FromFile(Path.Combine(RootProject.ProjectPath, StudentModel.PhotoPath));
                picPortrait.SizeMode = PictureBoxSizeMode.Zoom;
            }
            else
            {
                picPortrait.Image = Resource.photos_48px;
                picPortrait.SizeMode = PictureBoxSizeMode.CenterImage;
            }
        }

        private void dgvStudyInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvStudyInfo.CurrentCell is DataGridViewImageCell)
            {
                var columnName = dgvStudyInfo.CurrentCell.OwningColumn.Name;

                Console.WriteLine();
                if (columnName == "Invoice")
                {
                    var key = dgvStudyInfo.Rows[e.RowIndex].Cells["Id"].Value;
                    var row = tableEnrollment.Rows.Find(key);
                    var row2 = DisplayTable.Rows.Find(key);

                    var gender = ((Gender)StudentModel.Gender).ToString();

                    var courseName = row2["Course"];
                    var turn = row2["Turn"];
                    var time = row2["Time"];
                    var room = row2["Room"];
                    var teacher = row2["Teacher"];

                    var cost = row["CouseCost"];
                    var discount = row["Discount"];
                    var paid = row["PaidAmount"];

                    var userName = row.GetParentRow("EnrollmentUserRelation")["UserName"];
                    Console.WriteLine();

                    Register.frmPrintInvoice invoice = new Register.frmPrintInvoice(StudentModel.Name, gender, StudentModel.Dob, StudentModel.Phone, StudentModel.Facebook, StudentModel.Telegram, StudentModel.Email, StudentModel.Address, courseName.ToString(), turn.ToString(), time.ToString(), room.ToString(), teacher.ToString(), (double)cost, (double)discount, (double)paid, userName.ToString());

                    invoice.ShowDialog();
                }
                else if (columnName == "Edit")
                {
                    var currentRow = dgvStudyInfo.Rows[e.RowIndex];

                    frmEditStudentStatus status = new frmEditStudentStatus();
                    status.CurrentStatus = (string)currentRow.Cells["StudentStatus"].Value;
                    var state = status.ShowEditMode();

                    if (state == -1) return;

                    var enrollId = currentRow.Cells["Id"].Value;

                    var rows = tableEnrollment.Select($"Id = {enrollId}");


                    if (rows.Length > 0)
                    {
                        rows[0]["StudentStatus"] = state;
                        EnrollmentDal.UpdateEnrollmentStudentStatus();
                    }

                    Console.WriteLine("Status Number: {0}", state);
                }
            }
            else
            {
                Console.WriteLine(e.ColumnIndex + " " + e.RowIndex);
            }
        }

        private void btnNewClass_Click(object sender, EventArgs e)
        {
            Register.Wizard wizard = new Register.Wizard();
            wizard.IsOldStudent = true;
            var studentId = tableEnrollment.Rows[0]["StudentId"];
            wizard.StudentModel = StudentModel;
            wizard.ShowDialog();
            LoadStudentInfo();
        }

        //this.tableLayoutPanel1.Controls.Add(this.dtpDob, 1, 2);
        //this.tableLayoutPanel1.Controls.Add(this.cbGender, 1, 1);
    }
}
