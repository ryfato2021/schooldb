﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Room
{
    public partial class frmUpdateRoom : Form
    {
        public Room room;
        public DataRow Row;
        public bool isUpdate;

        public frmUpdateRoom()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (CheckEmptyTextBox(txtRoomName)) return;
            if (CheckEmptyTextBox(txtFloor)) return;

            if (!isUpdate)
            {
                var roomName = txtRoomName.Text;
                var maxStuNo = Convert.ToInt32(txtMaxStudentNo.Text);
                var floor = Convert.ToInt32(txtFloor.Text);
                var description = txtDescription.Text;

                room.RoomDal.AddRoom(roomName, maxStuNo, floor, description);
                room.RoomDal.Update();
                ClearTextBox();
            }
            else
            {
                Row["Name"] = txtRoomName.Text;
                Row["MaxStudentNo"] = txtMaxStudentNo.Text;
                Row["Floor"] = txtFloor.Text;
                Row["Description"] = txtDescription.Text;
                Row["ModifiedAt"] = DateTime.Now;
                room.RoomDal.Update();
                this.Close();
            }
            
        }
        private bool CheckEmptyTextBox(TextBox text)
        {
            if (string.IsNullOrWhiteSpace(text.Text))
            {
                text.Focus();
                return true;
            }
            return false;
        }
        private void ClearTextBox()
        {
            txtRoomName.Text = "";
            txtMaxStudentNo.Text = "";
            txtFloor.Text = "";
            txtDescription.Text = "";
        }
        public void BindingTextBox(DataRow Row)
        {
            txtRoomName.Text = Row["Name"].ToString();
            txtMaxStudentNo.Text = Row["MaxStudentNo"].ToString();
            txtFloor.Text = Row["Floor"].ToString();
            txtDescription.Text = Row["Description"].ToString();

            this.Row = Row;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
