﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Room
{
    public partial class Room : UserControl
    {
        public DAL.RoomDal RoomDal;
        public Room()
        {
            InitializeComponent();
        }
        public void SetUpRoom()
        {
            var table = RoomDal.GetAllRooms();

            dvgRoomInfo.DataSource = table;

            DataGridViewImageColumn dataGridViewImageColumnEdit = new DataGridViewImageColumn()
            {
                Name = "Edit",
                HeaderText = "Edit",
                Image = Resource.edit_20px,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Width = 75
            };
            DataGridViewImageColumn dataGridViewImageColumnDelete = new DataGridViewImageColumn()
            {
                Name = "Delete",
                HeaderText = "Delete",
                Image = Resource.delete_bin_20px,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Width = 75
            };
            dvgRoomInfo.Columns.Add(dataGridViewImageColumnEdit);
            dvgRoomInfo.Columns.Add(dataGridViewImageColumnDelete);

            dvgRoomInfo.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dvgRoomInfo.Columns["Id"].Width = 75;

            table.Columns["CreatedAt"].DefaultValue = DateTime.Now;
            table.Columns["ModifiedAt"].DefaultValue = DateTime.Now;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmUpdateRoom frmUpdateRoom = new frmUpdateRoom();
            frmUpdateRoom.room = this;
            frmUpdateRoom.ShowDialog();
        }

        private void dvgRoomInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //if( is DataGridViewImageColumn)

        }

        private void dvgRoomInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Console.WriteLine(dvgRoomInfo.CurrentCell.OwningColumn.Name);
            var key = dvgRoomInfo.SelectedCells[2].Value;
            var row = RoomDal.GetAllRooms().Rows.Find(key);
            if (row == null) return;

            var columnsName = dvgRoomInfo.CurrentCell.OwningColumn.Name;
            if (columnsName == "Edit")
            {
                frmUpdateRoom frmUpdateRoom = new frmUpdateRoom();
                frmUpdateRoom.isUpdate = true;
                frmUpdateRoom.btnAdd.Text = "Update";
                frmUpdateRoom.room = this;
                frmUpdateRoom.BindingTextBox(row);
                frmUpdateRoom.ShowDialog();
            }
            else if(columnsName == "Delete")
            {
                //if (MessageBox.Show("Are you to Delete this Room??", "Delete Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                //{
                //    RoomDal.DeleteRoom(row);
                //}

                foreach(var PrinterName in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                {
                    Console.WriteLine(PrinterName);
                }
            }
        }
    }
}
