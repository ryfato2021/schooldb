﻿using System.Data;
using System.Data.SqlClient;

namespace SchoolMG.DAL
{
    public class EnrollmentDal : ConnectionDB
    {
        SqlDataAdapter dataAdapter;
        DataSet dataSet;

        DataSet dataSetTmp;

        DataTable dataTable;
        public ClassDal ClassDal;
        public StudentDal StudentDal;
        public PaymentDal PaymentDal;

        public EnrollmentDal()
        {
            dataAdapter = new SqlDataAdapter();
            dataSet = new DataSet();
            dataTable = new DataTable();
            ClassDal = new ClassDal();
            StudentDal = new StudentDal();
            PaymentDal = new PaymentDal();

            dataSet = ClassDal._dataSet;
            dataSet.Tables.Add(StudentDal.GetAllStudents().Copy());

            //InitializeCommands();

            //DataRelation ClassEnrollmentRelation = new DataRelation("ClassEnrollmentRelation", dataSet.Tables["Classes"].Columns["Id"], dataTable.Columns["ClassId"]);
            //DataRelation StudentEnrollmentRelation = new DataRelation("StudentEnrollmentRelation", dataSet.Tables["Students"].Columns["Id"], dataSet.Tables["Students"].Columns["Id"]);

            //dataSet.Relations.Add(ClassEnrollmentRelation);
            //dataSet.Relations.Add(StudentEnrollmentRelation);

        }

        private void InitializeCommands()
        {
            dataAdapter.SelectCommand = new SqlCommand("Select * from Enrollments", _connection);

            dataAdapter.Fill(dataTable);
            dataTable.TableName = "Enrollments";
            dataSet.Tables.Add(dataTable);
        }
        public DataTable GetEnrollmentByStudent(object studentId)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            dataAdapter.SelectCommand = new SqlCommand("Select * from Enrollments Where StudentId=@StudentId", _connection);
            dataAdapter.SelectCommand.Parameters.Add("@StudentId", SqlDbType.Int, 0, "StudentId").Value = studentId;

            dataAdapter.UpdateCommand = new SqlCommand("Update Enrollments Set StudentStatus=@studentStatus Where Id=@enrollId", _connection);
            dataAdapter.UpdateCommand.Parameters.Add("@studentStatus", SqlDbType.Int, 0, "StudentStatus");
            dataAdapter.UpdateCommand.Parameters.Add("@enrollId", SqlDbType.Int, 0, "Id").SourceVersion = DataRowVersion.Original;

            dataSetTmp = dataSet.Copy();
            var table = new DataTable();
            dataAdapter.FillSchema(table, SchemaType.Source);
            dataAdapter.Fill(table);
            table.TableName = "Enrollments";

            dataSetTmp.Tables.Add(table);

            DataRelation ClassEnrollmentRelation = new DataRelation("ClassEnrollmentRelation", dataSetTmp.Tables["Classes"].Columns["Id"], dataSetTmp.Tables["Enrollments"].Columns["ClassId"]);

            if (!dataSetTmp.Relations.Contains("ClassEnrollmentRelation"))
                dataSetTmp.Relations.Add(ClassEnrollmentRelation);

            //dataSet = dataSetCopy;
            return dataSetTmp.Tables["Enrollments"];
        }
        public void UpdateEnrollmentStudentStatus()
        {
            try
            {
                var dataSetChange = dataSetTmp.GetChanges();
                dataAdapter.Update(dataSetChange, "Enrollments");
                dataSetTmp.AcceptChanges();
            }
            catch (SqlException ex)
            {
                dataSetTmp.RejectChanges();
                System.Console.WriteLine("Error: " + ex);
            }
        }
    }
}
