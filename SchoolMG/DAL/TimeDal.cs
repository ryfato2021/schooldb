﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.DAL
{
    public class TimeDal:ConnectionDB
    {
        DataTable DataTable = new DataTable();

        SqlDataAdapter DataAdapter = new SqlDataAdapter();

        public TimeDal()
        {
            InitializeCommands();
        }
        void InitializeCommands()
        {
            SqlCommand cmdSelect = new SqlCommand("Select * from Times", _connection);
            DataAdapter.SelectCommand = cmdSelect;

            DataAdapter.Fill(DataTable);
            DataAdapter.FillSchema(DataTable, SchemaType.Source);
            DataTable.TableName = "Times";
            DataTable.Columns.Add("FullTime");
            //DataTable.Columns["FullTime"].Expression = "Convert(StartTime, System.String)+'-'+Convert(EndTime, System.String)+PeriodTime";
            DataTable.Columns["FullTime"].Expression = "StartTime+'-'+EndTime+PeriodTime";
        }
        public DataTable GetAllTimes()
        {
            return DataTable;
        }
        //public DataTable
    }
}
