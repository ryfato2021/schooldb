﻿using SchoolMG.Enumerator;
using System;
using System.Data;
using System.Data.SqlClient​;

namespace SchoolMG.DAL
{
    public class StudentDal : ConnectionDB
    {
        SqlDataAdapter _dataAdapter;
        DataRelation _genderStudentRelation;
        DataSet _dataSet;
        public DataTable TableStudent;
        public StudentDal()
        {
            InitializeCommands();

            #region create Tmp Table
            //dataTable = new DataTable();
            //dataTable.Columns.Add("Id", typeof(int));
            //dataTable.Columns.Add("Name", typeof(string));
            //dataTable.Columns.Add("Gender", typeof(string));
            //dataTable.Columns.Add("DateOfBirth", typeof(DateTime));
            //dataTable.Columns.Add("Phone", typeof(string));
            //dataTable.Columns.Add("Facebook", typeof(string));
            //dataTable.Columns.Add("Telegram", typeof(string));
            //dataTable.Columns.Add("Email", typeof(string));
            //dataTable.Columns.Add("Address", typeof(string));

            //foreach(DataRow row in _dataSet.Tables[0].Rows)
            //{
            //    DataRow newRow = dataTable.NewRow();
            //    newRow["Id"] = row["Id"];
            //    newRow["Name"] = row["Name"];
            //    newRow["Gender"] = row.GetParentRow(_genderStudentRelation)["Gender"];
            //    newRow["DateOfBirth"] = row["DOB"];
            //    newRow["Phone"] = row["Phone"];
            //    newRow["Facebook"] = row["Facebook"];
            //    newRow["Telegram"] = row["Telegram"];
            //    newRow["Email"] = row["Email"];
            //    newRow["Address"] = row["Address"];

            //    dataTable.Rows.Add(newRow);
            //}
            #endregion
        }
        public int GetCurretStudentId()
        {
            SqlCommand cmd = new SqlCommand("SELECT IDENT_CURRENT('Students')", _connection);
            OpenConnection();
            var currentid = Convert.ToInt32(cmd.ExecuteScalar());
            CloseConnection();
            return currentid;
        }
        void InitializeCommands()
        {
            _dataAdapter = new SqlDataAdapter();
            _dataAdapter.SelectCommand = _connection.CreateCommand();
            _dataAdapter.SelectCommand.CommandText = "Select * From Students Order by Id Desc";
            _dataAdapter.SelectCommand.CommandType = CommandType.Text;

            //
            // Insert Command
            //
            _dataAdapter.InsertCommand = _connection.CreateCommand();
            _dataAdapter.InsertCommand.CommandText = "InsertStudent";
            _dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;
            // Add InsertCommand Parameter
            AddParam(_dataAdapter.InsertCommand, "name", SqlDbType.NVarChar);
            AddParam(_dataAdapter.InsertCommand, "gender", SqlDbType.Int);
            AddParam(_dataAdapter.InsertCommand, "dob", SqlDbType.DateTime);
            AddParam(_dataAdapter.InsertCommand, "phone", SqlDbType.VarChar);
            AddParam(_dataAdapter.InsertCommand, "facebook", SqlDbType.NVarChar);
            AddParam(_dataAdapter.InsertCommand, "telegram", SqlDbType.VarChar);
            AddParam(_dataAdapter.InsertCommand, "email", SqlDbType.VarChar);
            AddParam(_dataAdapter.InsertCommand, "address", SqlDbType.NVarChar);
            AddParam(_dataAdapter.InsertCommand, "photo", SqlDbType.NVarChar);

            //
            // Update Command
            //
            _dataAdapter.UpdateCommand = new SqlCommand("UpdateStudent", _connection);
            _dataAdapter.UpdateCommand.CommandType = CommandType.StoredProcedure;
            // Add UpdateCommand Parameter
            _dataAdapter.UpdateCommand.Parameters.Add("@name", SqlDbType.NVarChar, 50, "Name");
            _dataAdapter.UpdateCommand.Parameters.Add("@gender", SqlDbType.Int, 0, "Gender");
            _dataAdapter.UpdateCommand.Parameters.Add("@dob", SqlDbType.Date, 0, "DOB");
            _dataAdapter.UpdateCommand.Parameters.Add("@phone", SqlDbType.VarChar, 50, "Phone");
            _dataAdapter.UpdateCommand.Parameters.Add("@facebook", SqlDbType.NVarChar, 50, "Facebook");
            _dataAdapter.UpdateCommand.Parameters.Add("@telegram", SqlDbType.VarChar, 50, "Telegram");
            _dataAdapter.UpdateCommand.Parameters.Add("@email", SqlDbType.VarChar, 50, "Email");
            _dataAdapter.UpdateCommand.Parameters.Add("@address", SqlDbType.NVarChar, 1000, "Address");
            _dataAdapter.UpdateCommand.Parameters.Add("@photo", SqlDbType.NVarChar, 200, "Photo");
            _dataAdapter.UpdateCommand.Parameters.Add("@id", SqlDbType.Int);
            _dataAdapter.UpdateCommand.Parameters["@id"].SourceColumn = "Id";
            _dataAdapter.UpdateCommand.Parameters["@id"].SourceVersion = DataRowVersion.Original;

            _dataSet = new DataSet();
            _dataAdapter.FillSchema(_dataSet, SchemaType.Source, "Students");
            _dataAdapter.Fill(_dataSet, "Students");
            TableStudent = _dataSet.Tables["Students"];
        }
        private void AddParam(SqlCommand cmd, string paramName, SqlDbType sqlDbType)
        {
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@" + paramName, SqlDbType = sqlDbType, Direction = ParameterDirection.Input, SourceColumn = paramName });
        }

        public DataTable GetAllStudents()
        {
            return TableStudent;
        }
        public DataRow InsertStudent(string name, int gender, DateTime dob, string phone, string facebook, string telegram, string email, string address, string photo)
        {
            DataRow newRow = _dataSet.Tables["Students"].NewRow();
            //newRow["Id"] = GetCurretStudentId() + 1;
            newRow["Name"] = name;
            newRow["Gender"] = (int)gender;
            newRow["DOB"] = dob;
            newRow["Phone"] = ValidStringValue(phone);
            newRow["Facebook"] = ValidStringValue(facebook);
            newRow["Telegram"] = ValidStringValue(telegram);
            newRow["Email"] = ValidStringValue(email);
            newRow["Address"] = ValidStringValue(address);
            newRow["Photo"] = ValidStringValue(photo);

            _dataSet.Tables["Students"].Rows.InsertAt(newRow, 0);

            DataSet dataSetChange = _dataSet.GetChanges(DataRowState.Added);

            try
            {
                _dataAdapter.Update(dataSetChange, "Students");
                _dataSet.AcceptChanges();
                //dataTable.AcceptChanges();
            }
            catch (SqlException ex)
            {
                _dataSet.RejectChanges();
                //dataTable.RejectChanges();
                Console.WriteLine("Error: {0}", ex);
            }
            Console.WriteLine("Current StudentId: {0}", GetCurretStudentId());
            return newRow;
        }
        private string ValidStringValue(string str)
        {
            if (string.IsNullOrWhiteSpace(str)) return null;
            return str;
        }
        public void UpdateStudent(string name, Gender gender, DateTime dob, string phone, string facebook, string telegram, string email, string address, string photo, int id)
        {
            var rows = TableStudent.Select($"Id={id}");
            if (rows.Length > 0)
            {
                DataRow row = rows[0];
                row["Name"] = name;
                row["Gender"] = (int)gender;
                row["DOB"] = dob;
                row["Phone"] = phone;
                row["Facebook"] = ValidStringValue(facebook);
                row["Telegram"] = ValidStringValue(telegram);
                row["Email"] = ValidStringValue(email);
                row["Address"] = ValidStringValue(address);
                row["Photo"] = ValidStringValue(photo);
            }
            try
            {
                var tmp = _dataSet.GetChanges();
                _dataAdapter.Update(_dataSet, "Students");
                _dataSet.AcceptChanges();
            }
            catch (Exception ex)
            {
                _dataSet.RejectChanges();
            }
        }

        public DataRow GetOneStudentById(int Id)
        {
            SqlCommand cmd = new SqlCommand("Select * from Students Where Id = @Id", _connection);
            cmd.Parameters.AddWithValue("@id", Id);

            OpenConnection();
            using (var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
            {
                if (reader.HasRows)
                {
                    reader.Read();

                    DataRow newRow = _dataSet.Tables["Students"].NewRow();
                    newRow["Id"] = reader["Id"];
                    newRow["Name"] = reader["Name"];
                    newRow["Gender"] = reader["Gender"];
                    newRow["DOB"] = reader["DOB"];
                    newRow["Phone"] = reader["Phone"];
                    newRow["Facebook"] = reader["Facebook"];
                    newRow["Telegram"] = reader["Telegram"];
                    newRow["Email"] = reader["Email"];
                    newRow["Address"] = reader["Address"];
                    newRow["Photo"] = reader["Photo"];

                    return newRow;
                }
            }
            return null;
        }
        //public void AddToDisplayTable(DataRow row)
        //{
        //    DataRow newRow = dataTable.NewRow();
        //    newRow["Id"] = row["Id"];
        //    newRow["Name"] = row["Name"];
        //    newRow["Gender"] = row.GetParentRow(_genderStudentRelation)["Gender"];
        //    newRow["DateOfBirth"] = row["DOB"];
        //    newRow["Phone"] = row["Phone"];
        //    newRow["Facebook"] = row["Facebook"];
        //    newRow["Telegram"] = row["Telegram"];
        //    newRow["Email"] = row["Email"];
        //    newRow["Address"] = row["Address"];

        //    dataTable.Rows.InsertAt(newRow, 0);
        //}
    }
}
