﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SchoolMG.DAL
{
    public class ConnectionDB
    {
        protected SqlConnection _connection;
        public ConnectionDB()
        {
            _connection = new SqlConnection(@"Data source=.\SQLEXPRESS; Initial Catalog = SchoolDB; User Id=sa; password=sa");
        }
        protected void OpenConnection()
        {
            if (_connection.State != System.Data.ConnectionState.Open)
                _connection.Open();
        }
        protected void CloseConnection()
        {
            if (_connection.State != System.Data.ConnectionState.Closed)
                _connection.Close();
        }
    }
}
