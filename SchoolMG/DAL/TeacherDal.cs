﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SchoolMG;

namespace SchoolMG.DAL
{
    public class TeacherDal:ConnectionDB
    {
        SqlDataAdapter DataAdapter;
        DataTable DataTable;

        public TeacherDal()
        {
            InitialCommands();
        }
        void InitialCommands()
        {
            DataAdapter = new SqlDataAdapter();
            DataTable = new DataTable();

            DataAdapter.SelectCommand = new SqlCommand("Select * from Teachers", _connection);

            DataAdapter.FillSchema(DataTable, SchemaType.Source);
            DataAdapter.Fill(DataTable);
            DataTable.TableName = "Teachers";
        }
        public DataTable GetAllTeachers()
        {
            return DataTable;
        }
    }
}
