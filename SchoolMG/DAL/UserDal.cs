﻿using System.Data;
using System.Data.SqlClient;

namespace SchoolMG.DAL
{
    public class UserDal : ConnectionDB
    {
        public DataTable DataTable = new DataTable();
        private int UserId { get; set; }
        public UserDal()
        {
            using (var adatapter = new SqlDataAdapter("Select * from Users", _connection))
            {
                adatapter.FillSchema(DataTable, SchemaType.Source);
                adatapter.Fill(DataTable);
                DataTable.TableName = "Users";
            }
        }

        public DataTable GetUsers()
        {
            return DataTable;
        }


        public int Login(string username, string password)
        {
            SqlCommand cmd = new SqlCommand("SP_Login", _connection);
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@userName",
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Value = username,
                Size = 50,
                Direction = System.Data.ParameterDirection.Input
            });
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@password",
                SqlDbType = System.Data.SqlDbType.NVarChar,
                Value = password,
                Size = 50,
                Direction = System.Data.ParameterDirection.Input
            });
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@result",
                SqlDbType = System.Data.SqlDbType.Int,
                Size = 50,
                Direction = System.Data.ParameterDirection.Output
            });
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            OpenConnection();
            try
            {
                cmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            CloseConnection();

            UserId = (int)cmd.Parameters["@result"].Value;

            return UserId;
        }
    }
}
