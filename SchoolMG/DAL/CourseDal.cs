﻿using SchoolMG.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.DAL
{
    public class CourseDal:ConnectionDB
    {
        SqlDataAdapter _dataAdapter;
        DataSet _dataSet;
        DataTable dataTable;

        public CourseDal()
        {
            InitializeCommands();
        }
        private void InitializeCommands()
        {
            _dataAdapter = new SqlDataAdapter();
            _dataSet = new DataSet();
            dataTable = new DataTable();

            SqlCommand cmdSelect = new SqlCommand("Select * from Courses", _connection);
            _dataAdapter.SelectCommand = cmdSelect;

            _dataAdapter.InsertCommand = new SqlCommand("InsertCourse", _connection);
            _dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;
            _dataAdapter.InsertCommand.Parameters.Add("@cost", SqlDbType.Float, 0, "Cost");
            _dataAdapter.InsertCommand.Parameters.Add("@name", SqlDbType.NVarChar, 50, "Name");
            _dataAdapter.InsertCommand.Parameters.Add("@duration", SqlDbType.Int, 0, "Duration");
            _dataAdapter.InsertCommand.Parameters.Add("@description", SqlDbType.NVarChar, 250, "Description");
            _dataAdapter.InsertCommand.Parameters.Add("@picture", SqlDbType.NVarChar, 250, "Picture");

            var updateCmd = _dataAdapter.UpdateCommand = new SqlCommand("UpdateCourse", _connection);
            updateCmd.CommandType = CommandType.StoredProcedure;
            updateCmd.Parameters.Add("@name", SqlDbType.NVarChar, 50, "Name");
            updateCmd.Parameters.Add("@cost", SqlDbType.Float, 0, "Cost");
            updateCmd.Parameters.Add("@duration", SqlDbType.Int, 0, "Duration");
            updateCmd.Parameters.Add("@description", SqlDbType.NVarChar, 250, "Description");
            updateCmd.Parameters.Add("@picture", SqlDbType.NVarChar, 250, "Picture");
            updateCmd.Parameters.Add("@id", SqlDbType.Int, 0, "Id").SourceVersion = DataRowVersion.Original;

            SqlCommand deleteCmd = new SqlCommand("Delete From Courses Where Id=@Id", _connection);
            deleteCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").SourceVersion = DataRowVersion.Original;

            _dataAdapter.DeleteCommand = deleteCmd;

            _dataAdapter.FillSchema(dataTable, SchemaType.Source);
            _dataAdapter.Fill(dataTable);
            dataTable.TableName = "Courses";
        }
        public DataTable GetAllCourses()
        {
            return dataTable;
        }
        public void AddCourse(string name, double cost, int duration, string description, string picture)
        {
            DataRow newRow = dataTable.NewRow();
            newRow["Name"] = name;
            newRow["Cost"] = cost;
            newRow["Duration"] = duration;
            newRow["description"] = description;
            newRow["Picture"] = picture;

            dataTable.Rows.Add(newRow);
            try
            {
                _dataAdapter.Update(dataTable);
                dataTable.AcceptChanges();
            }catch(SqlException ex)
            {
                Console.WriteLine("Error: " + ex);
                dataTable.RejectChanges();
            }
        }

        public void UpdateCourse()
        {

            var table = dataTable.GetChanges();

            try
            {
            _dataAdapter.Update(table);
                dataTable.AcceptChanges();
            }catch(SqlException ex)
            {
                dataTable.RejectChanges();
                Console.WriteLine("Error: " + ex);
            }
        }
        public void Delete()
        {
            try
            {
                var table = dataTable.GetChanges(DataRowState.Deleted);
                _dataAdapter.Update(table);
                dataTable.AcceptChanges();
            }
            catch(SqlException ex)
            {
                System.Windows.Forms.MessageBox.Show($"You Can not Delete this Course! {ex}", "Delete Warning", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);

            }
           

        }
        public CourseModel GetCourseById(object courseId)
        {
            CourseModel courseModel = null;
            var row = dataTable.Rows.Find(courseId);
            if (row == null) return null;

            courseModel.Id = (int)row["Id"];
            courseModel.Name = row["Name"].ToString() ;
            courseModel.Cost = Convert.ToDouble(row["Cost"]);
            courseModel.Duration = Convert.ToInt32(row["Duration"]);
            courseModel.Description = row["Description"].ToString();
            courseModel.Picture = row["Picture"].ToString();

            return courseModel;
        }
    }
}
