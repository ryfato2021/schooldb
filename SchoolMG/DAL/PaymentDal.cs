﻿using System.Data;
using System.Data.SqlClient;

namespace SchoolMG.DAL
{
    public class PaymentDal : ConnectionDB
    {
        SqlDataAdapter _dataAdapter = new SqlDataAdapter();
        DataSet _dataSet = new DataSet() ;
        DataTable _dataTable = new DataTable();

        public PaymentDal()
        {
            _dataAdapter = new SqlDataAdapter();
            _dataAdapter.SelectCommand = new SqlCommand("Select * from Enrollments", _connection);
            _dataAdapter.SelectCommand.CommandType = CommandType.Text;

            _dataAdapter.FillSchema(_dataTable, SchemaType.Source);
            _dataAdapter.Fill(_dataTable);
            _dataTable.TableName = "Enrollments";

        }
        void AddParam(SqlCommand cmd, string columnName, SqlDbType sqlDbType, object Value, int size = 50, ParameterDirection direction = ParameterDirection.InputOutput)
        {
            cmd.Parameters.Add(new SqlParameter()
            {
                ParameterName = "@" + columnName,
                SqlDbType = sqlDbType,
                Size = size,
                Direction = ParameterDirection.Input,
            });
        }
       
        public bool CreateStudentEnrollment(int studentId, int classId, int studentStatus, double discount, double paidAmount, string note, int userId)
        {
            SqlCommand cmdInsert = new SqlCommand("dbo.Payment", _connection);
            cmdInsert.CommandType = CommandType.StoredProcedure;
            cmdInsert.Parameters.AddWithValue("@studentId", studentId);
            cmdInsert.Parameters.AddWithValue("@classId", classId);
            cmdInsert.Parameters.AddWithValue("@studentStatus", studentStatus);
            cmdInsert.Parameters.AddWithValue("@discount", discount);
            cmdInsert.Parameters.AddWithValue("@paidamount", paidAmount);
            cmdInsert.Parameters.AddWithValue("@note", note);
            cmdInsert.Parameters.AddWithValue("@userId", userId);
            cmdInsert.Parameters["@note"].SqlDbType = SqlDbType.NVarChar;
            cmdInsert.Parameters["@note"].Size = 250;
            
            try
            {
                OpenConnection();
                cmdInsert.ExecuteNonQuery();

                System.Console.WriteLine("Note: {0}", cmdInsert.Parameters["@note"].Value);
                
            }catch(SqlException ex)
            {
                System.Console.WriteLine("Error: {0}", ex);
                System.Console.WriteLine("Note: {0}", cmdInsert.Parameters["@note"].Value);
            }
            finally
            {
                CloseConnection();
            }
            return true;
        }
        public DataTable GetEnrolledStudentByClass(int classId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand($"Select * from VEnrollment Where ClassId={classId}", _connection);

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.FillSchema(dt, SchemaType.Source);
            adapter.Fill(dt);
            dt.TableName = "EnrolledStudentByClass";
            return dt;
        }
        public void UpdateStudentStatus(int state, int enrollId)
        {
            SqlCommand cmd = new SqlCommand($"Update Enrollments Set StudentStatus={state} Where Id={enrollId}");
            cmd.Connection = _connection;
            OpenConnection();
            cmd.ExecuteNonQuery();
            CloseConnection();
        }
    }
}
