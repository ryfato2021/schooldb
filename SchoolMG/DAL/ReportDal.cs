﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.DAL
{
    public class ReportDal:ConnectionDB
    {
        public DataTable GetSellReport()
        {
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter("Select * from SellReport", _connection);
            adapter.Fill(dt);
            dt.TableName = "SellReport";

            return dt;
        }
        public DataTable GetSellReportByDate(DateTime fromDate, DateTime toDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("GetSellReportByDate", _connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@fromDate", fromDate));
            cmd.Parameters.Add(new SqlParameter("@toDate", toDate));

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            dt.TableName = "SellReportByDate";
            return dt;
        }
        public DataTable GetStudentReportByClass(int classId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand($"Select * from VEnrollment Where ClassId={classId}", _connection);

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            dt.TableName = "StudentReportByClass";
            return dt;
        }
    }
}
