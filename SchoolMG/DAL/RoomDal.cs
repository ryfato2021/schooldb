﻿using System.Data;
using System.Data.SqlClient;

namespace SchoolMG.DAL
{
    public class RoomDal : ConnectionDB
    {
        SqlDataAdapter dataAdapter;
        DataTable DataTable;

        public RoomDal()
        {
            InititalizeCommands();
        }
        void InititalizeCommands()
        {
            dataAdapter = new SqlDataAdapter();
            DataTable = new DataTable();

            dataAdapter.SelectCommand = new SqlCommand("Select * from Rooms", _connection);

            var insertCmd = dataAdapter.InsertCommand = new SqlCommand("AddRoom", _connection);
            dataAdapter.InsertCommand.CommandType = CommandType.StoredProcedure;
            insertCmd.Parameters.Add("@name", SqlDbType.NVarChar, 50, "Name");
            insertCmd.Parameters.Add("@maxStuNo", SqlDbType.NVarChar, 50, "MaxStudentNo");
            insertCmd.Parameters.Add("@floor", SqlDbType.Int, 0, "Floor");
            insertCmd.Parameters.Add("@description", SqlDbType.NVarChar, 50, "Description");

            var updateCmd = dataAdapter.UpdateCommand = new SqlCommand("UpdateRoom", _connection);
            updateCmd.CommandType = CommandType.StoredProcedure;
            updateCmd.Parameters.Add("@name", SqlDbType.NVarChar, 50, "Name");
            updateCmd.Parameters.Add("@maxStuNo", SqlDbType.Int, 0, "MaxStudentNo");
            updateCmd.Parameters.Add("@floor", SqlDbType.Int, 0, "Floor");
            updateCmd.Parameters.Add("@description", SqlDbType.NVarChar, 250, "Description");
            updateCmd.Parameters.Add("@id", SqlDbType.Int, 0, "Id").SourceVersion = DataRowVersion.Original;

            var deleteCmd = new SqlCommand("Delete From Rooms Where id=@id", _connection);
            deleteCmd.Parameters.Add("@id", SqlDbType.Int, 0, "Id").SourceVersion = DataRowVersion.Original;
            dataAdapter.DeleteCommand = deleteCmd;


            dataAdapter.FillSchema(DataTable, SchemaType.Source);
            dataAdapter.Fill(DataTable);
            DataTable.TableName = "Rooms";
        }
        public DataTable GetAllRooms()
        {
            return DataTable;
        }

        public void Update()
        {
            try
            {
                var table = DataTable.GetChanges();
                dataAdapter.Update(table);
                DataTable.AcceptChanges() ;
            }catch(SqlException ex)
            {
                DataTable.RejectChanges();
                System.Console.WriteLine("Erro: {0}", ex);
            }

        }
        public void AddRoom(string name, int maxStuNo, int floor, string description)
        {
            DataRow row = DataTable.NewRow();
            row["Name"] = name;
            row["MaxStudentNo"] = maxStuNo;
            row["Floor"] = floor;
            row["Description"] = description;

            DataTable.Rows.Add(row);
        }
        public void DeleteRoom(DataRow row)
        {
            try
            {
                row.Delete();
                Update();
            }catch(SqlException ex)
            {
                System.Windows.Forms.MessageBox.Show("You cannot Delete this Room");
            }
        }
    }
}
