﻿using SchoolMG.Enumerator;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SchoolMG.DAL
{
    public class ClassDal : ConnectionDB
    {
        SqlDataAdapter _dataAdpaterClass;

        public CourseDal CourseDal;
        public TimeDal TimeDal;
        public RoomDal RoomDal;
        public TeacherDal TeacherDal;

        DataTable tableClass = new DataTable();
        public DataSet _dataSet = new DataSet();

        public DataTable tableDisplay = new DataTable("DisplayAllClasses");


        public ClassDal()
        {
            InitializeClassesCommands();

            // Implement DataColumns for tableDisplay
            tableDisplay.Columns.Add("ClassId", typeof(int));
            tableDisplay.Columns.Add("Course", typeof(string));
            tableDisplay.Columns.Add("Turn", typeof(string));
            tableDisplay.Columns.Add("Time", typeof(string));
            tableDisplay.Columns.Add("Room", typeof(string));
            tableDisplay.Columns.Add("Teacher", typeof(string));
            tableDisplay.Columns.Add("ClassStatus", typeof(string));

            SetDisplayAllClasses(DataRowState.Unchanged);

        }

        void InitializeClassesCommands()
        {
            CourseDal = new CourseDal();
            TimeDal = new TimeDal();
            RoomDal = new RoomDal();
            TeacherDal = new TeacherDal();

            DataTable tableCourse = CourseDal.GetAllCourses();
            DataTable tableTime = TimeDal.GetAllTimes();
            DataTable tableRoom = RoomDal.GetAllRooms();
            DataTable tableTeacher = TeacherDal.GetAllTeachers();


            _dataAdpaterClass = new SqlDataAdapter();
            _dataAdpaterClass.SelectCommand = _connection.CreateCommand();
            _dataAdpaterClass.SelectCommand.CommandText = "Select * From Classes";
            _dataAdpaterClass.SelectCommand.CommandType = CommandType.Text;
            _dataAdpaterClass.Fill(_dataSet, "Classes");
            _dataAdpaterClass.FillSchema(_dataSet, SchemaType.Source, "Classes");

            tableClass = _dataSet.Tables["Classes"];

            // -- Create Table Turn

            DataTable tableTurn = new DataTable("Turn");
            tableTurn.Columns.Add("TurnId", typeof(int));
            tableTurn.Columns.Add("Turn", typeof(string));
            tableTurn.Rows.Add(0, "Weekday");
            tableTurn.Rows.Add(1, "Weekend");

            _dataSet.Tables.Add(tableCourse);
            _dataSet.Tables.Add(tableTime);
            _dataSet.Tables.Add(tableRoom);
            _dataSet.Tables.Add(tableTeacher);
            _dataSet.Tables.Add(tableTurn);



            DataRelation CourseClassRelation = new DataRelation("CourseClassRelation", tableCourse.Columns["Id"], tableClass.Columns["CourseId"], false);

            DataRelation TimeClassRelation = new DataRelation("TimeClassRelation", tableTime.Columns["Id"], tableClass.Columns["TimeId"], false);

            DataRelation RoomClassRelation = new DataRelation("RoomClassRelation", tableRoom.Columns["Id"], tableClass.Columns["RoomId"], false);

            DataRelation TeacherClassRelation = new DataRelation("TeacherClassRelation", tableTeacher.Columns["Id"], tableClass.Columns["TeacherId"], false);

            DataRelation TurnClassRelation = new DataRelation("TurnClassRelation", tableTurn.Columns[0], tableClass.Columns["Turn"]);


            _dataSet.Relations.Add(CourseClassRelation);
            _dataSet.Relations.Add(TimeClassRelation);
            _dataSet.Relations.Add(RoomClassRelation);
            _dataSet.Relations.Add(TeacherClassRelation);
            _dataSet.Relations.Add(TurnClassRelation);



        }

        public void TestDisplay()
        {
            Console.WriteLine(" *** All Constaints *** ");
            foreach (Constraint con in tableClass.Constraints)
            {
                Console.WriteLine("Constrints: {0}", con.ConstraintName);
            }
            Console.WriteLine(" *** Show All Table *** ");

            foreach (DataTable table in _dataSet.Tables)
            {
                Console.WriteLine(table.TableName);
            }

            foreach (DataRelation rel in _dataSet.Relations)
            {
                Console.WriteLine(rel.RelationName);
            }
        }

        public void DisplayAllClassesInConsole()
        {



        }
        private void SetDisplayAllClasses(DataRowState dataRowState)
        {
            foreach (DataRow row in tableClass.Rows)
            {
                if (row.RowState == dataRowState)
                {
                    var courseRow = row.GetParentRow("CourseClassRelation");
                    var turnRow = row.GetParentRow("TurnClassRelation");
                    var timeRow = row.GetParentRow("TimeClassRelation");
                    var roomRow = row.GetParentRow("RoomClassRelation");
                    var teacherRow = row.GetParentRow("TeacherClassRelation");

                    DataRow newRow = tableDisplay.NewRow();
                    newRow["ClassId"] = row["Id"];
                    newRow["Course"] = courseRow["Name"];
                    newRow["Turn"] = turnRow["Turn"];
                    newRow["Time"] = timeRow["FullTime"];
                    newRow["Room"] = roomRow["Name"];
                    newRow["Teacher"] = teacherRow["Name"];
                    newRow["ClassStatus"] = (ClassState)row["Status"];

                    tableDisplay.Rows.Add(newRow);
                }
            }
        }
        public DataTable GetAllClases()
        {
            return tableClass;
        }

        public int GetClassId(int courseId, int turnId, int timeId, int roomId)
        {
            SqlCommand cmd = new SqlCommand("GetClassByCourseTurnTimeAndRoom", _connection);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@courseId", courseId);
            cmd.Parameters.AddWithValue("@turnId", turnId);
            cmd.Parameters.AddWithValue("@timeId", timeId);
            cmd.Parameters.AddWithValue("@roomId", roomId);

            OpenConnection();
            using (var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
            {
                if (reader.HasRows)
                {
                    reader.Read();
                    return (int)reader["Id"];
                }
            }
            return -1;

        }

        public DataTable GetClasses()
        {
            DataTable table = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM VGetAllClasses", _connection);
            da.Fill(table);
            table.TableName = "Classes";

            return table;
        }

        public DataTable GetAvailaCourseoofClasses()
        {
            DataTable dataTable = CourseDal.GetAllCourses().Clone();
            DataTable tbCourse = CourseDal.GetAllCourses();
            foreach (DataRow pRow in tbCourse.Rows)
            {
                DataRow[] cRow = pRow.GetChildRows("CourseClassRelation");
                if (cRow.Length == 0) continue;
                dataTable.ImportRow(pRow);
            }
            return dataTable;
        }
        public DataTable GetAvailableTurn(int courseId)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("TurnId", typeof(int));
            dataTable.Columns.Add("Turn", typeof(string));


            foreach (DataRow pRow in _dataSet.Tables["Turn"].Rows)
            {
                var cRows = pRow.GetChildRows("TurnClassRelation");
                var data = cRows.CopyToDataTable().Select($"CourseId = {courseId}");
                if (data.Length > 0)
                {
                    dataTable.ImportRow(pRow);
                }
            }
            return dataTable;

        }
        public DataTable GetAvailableTimeOfClass(int courseId, int turn)
        {
            DataTable dataTable = TimeDal.GetAllTimes().Clone();
            var selectRows = tableClass.Select($"CourseId = {courseId} and Turn = {turn}");
            foreach (DataRow cRow in selectRows)
            {
                DataRow pRow = cRow.GetParentRow("TimeClassRelation");
                dataTable.ImportRow(pRow);
            }

            return dataTable;
        }
        public DataTable GetAvailableRoomOfClass(int courseId, int turn, int timeId)
        {
            DataTable dataTable = RoomDal.GetAllRooms().Clone();
            var selectRows = tableClass.Select($"CourseId = {courseId} and Turn = {turn} and TimeId = {timeId}");
            foreach (DataRow cRow in selectRows)
            {
                DataRow pRow = cRow.GetParentRow("RoomClassRelation");
                dataTable.ImportRow(pRow);
            }
            return dataTable;
        }
        public DataTable GetAvailableTeacherOfClass(int turn, int timeId, int roomId)
        {
            DataTable dataTable = TeacherDal.GetAllTeachers().Clone();
            var selectRows = tableClass.Select($"Turn = {turn} and TimeId = {timeId} and RoomId={roomId}");
            foreach (DataRow cRow in selectRows)
            {
                var pRow = cRow.GetParentRow("TeacherClassRelation");
                dataTable.ImportRow(pRow);
            }
            return dataTable;
        }


    }
}
