﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolMG.Course;
using SchoolMG.DAL;
using SchoolMG.Register;

namespace SchoolMG
{
    public partial class StartUp : UserControl
    {
        public SchoolMG.DAL.StudentDal StudentDal;
        public SchoolMG.Register.Wizard Wizard;
        public ClassDal classDAL;
        public CourseDal CourseDal;
        public Course.Course Course;

        public Home Home;

        public StartUp()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            Wizard = new Wizard();
            Wizard.UserId = Home.UserId;
            Wizard.StudentDal = StudentDal;
            Wizard.ShowDialog();
        }

        private void btnCourse_Click(object sender, EventArgs e)
        {
            Course = new Course.Course();
            classDAL = new ClassDal();
            Course.courseDal = classDAL.CourseDal;
            Course.SetUpCourse();
            Course.AutoSize = true;
            Course.Dock = DockStyle.Fill;

            Home.ShowUp(Course);
        }

        private void btnRoom_Click(object sender, EventArgs e)
        {
            Room.Room room = new Room.Room();
            classDAL = new ClassDal();
            room.RoomDal = classDAL.RoomDal;
            room.SetUpRoom();
            room.AutoSize = true;
            room.Dock = DockStyle.Fill;

            Home.ShowUp(room);
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            Report.SellReport report = new Report.SellReport();
            report.Home = Home;
            Home.ShowUp(report);
        }

        private void btnStudentInfo_Click(object sender, EventArgs e)
        {

        }

        private void btnClass_Click(object sender, EventArgs e)
        {

        }
    }
}
