﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolMG.DAL;

namespace SchoolMG
{
    public partial class frmLogIn : Form
    {
        public UserDal user = new UserDal();
        public int userId ;
        public frmLogIn()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            //    lblPasswordError.Text = "*";
            //    lblUsernameError.Text = "*";
            //if (string.IsNullOrWhiteSpace(txtUserName.Text))
            //{
            //    Console.WriteLine("Program is Terminated!!");
            //    lblUsernameError.Text = "*Required Input!!";
            //    return;
            //}
            bool isEmptyUserName = CheckEmptyText(txtUserName, lblUsernameError);
            bool isEmptyPassword = CheckEmptyText(txtPassword, lblPasswordError);

            if (isEmptyPassword || isEmptyUserName)
                return;

            userId = user.Login(txtUserName.Text, txtPassword.Text);

            if(userId > 0)
            {
                this.Close();
            }
            else
            {
                if(userId == -1)
                {
                    lblResult.Text = "Invalid Username";
                }
                if(userId == -2)
                {
                    lblResult.Text = "Invalid Password";
                }
            }

        }
        bool CheckEmptyText(TextBox text, Label label)
        {
            if (string.IsNullOrWhiteSpace(text.Text))
            {
                text.Focus();
                label.Text = "*Required Input";
                return true;
            }
            label.Text = "*";
            return false;
        }
        void setDefaultLabel(Label label)
        {
            label.Text = "*";
        }
        public int ShowLoginForm()
        {
            this.ShowDialog();
            return userId;
        }
    }
}
