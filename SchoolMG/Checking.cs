﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG
{
    public static class Checking
    {
        public static bool IsKhmerDigit(char ch)
        {
            for (int i = (int)'០'; i <= (int)'៩'; i++)
            {
                if (ch == (char)i) return true;
            }
            return false;
        }
        public static bool IsKhmerLetter(char ch)
        {
            int start = Convert.ToInt32("1780", 16);
            int end = Convert.ToInt32("17DD", 16);

            for (int i = start; i <= end; i++)
            {
                if (ch == (char)i) return true;
            }
            return false;
        }
    }
}
