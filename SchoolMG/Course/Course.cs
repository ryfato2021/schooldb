﻿using SchoolMG.DAL;
using System;
using System.Windows.Forms;

namespace SchoolMG.Course
{
    public partial class Course : UserControl
    {
        public CourseDal courseDal = new CourseDal();
        public Course()
        {
            InitializeComponent();
        }
        public void SetUpCourse()
        {
            var table = courseDal.GetAllCourses();

            dgvCourseInfo.DataSource = table;

            DataGridViewImageColumn dataGridViewImageColumnEdit = new DataGridViewImageColumn()
            {
                Name = "Edit",
                HeaderText = "Edit",
                Image = Resource.edit_20px,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Width = 75
            };
            DataGridViewImageColumn dataGridViewImageColumnDelete = new DataGridViewImageColumn()
            {
                Name = "Delete",
                HeaderText = "Delete",
                Image = Resource.delete_bin_20px,
                AutoSizeMode = DataGridViewAutoSizeColumnMode.None,
                Width = 75
            };
            dgvCourseInfo.Columns.Add(dataGridViewImageColumnEdit);
            dgvCourseInfo.Columns.Add(dataGridViewImageColumnDelete);

            dgvCourseInfo.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgvCourseInfo.Columns["Id"].Width = 75;

            table.Columns["CreatedAt"].DefaultValue = DateTime.Now;
            table.Columns["ModifiedAt"].DefaultValue = DateTime.Now;

        }

        private void dgvCourseInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Console.WriteLine("Column: " + e.ColumnIndex);
            Console.WriteLine("Row: " + e.RowIndex);

            switch (e.ColumnIndex)
            {
                case 0:
                    var key = dgvCourseInfo.SelectedCells[2].Value;

                    var row = courseDal.GetAllCourses().Rows.Find(key);
                    if (row != null)
                    {
                        frmAddCourse updateCourse = new frmAddCourse();
                        updateCourse.isUpdate = true;
                        updateCourse.CourseDal = this.courseDal;
                        updateCourse.Row = row;
                        updateCourse.BindingTextBox();
                        updateCourse.btnAdd.Text = "Update";
                        updateCourse.ShowDialog();
                    }
                    break;
                case 1:
                    key = dgvCourseInfo.SelectedCells[2].Value;
                    row = courseDal.GetAllCourses().Rows.Find(key);
                    if (row != null)
                    {
                        if (MessageBox.Show("Are you sure to delete this Course?", "Deletion Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            row.Delete();
                            courseDal.Delete();
                        }
                    }
                    break;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmAddCourse frmAddCourse = new frmAddCourse();
            frmAddCourse.CourseDal = this.courseDal;

            frmAddCourse.ShowDialog();
        }
    }
}
