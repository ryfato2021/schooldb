﻿using SchoolMG.DAL;
using SchoolMG.Model;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SchoolMG.Course
{
    public partial class frmAddCourse : Form
    {
        public CourseModel courseModel;
        public CourseDal CourseDal;
        public bool isUpdate;
        public DataRow Row;

        public string Picture { get { return _isChosePicture ? (txtCourseName.Text + _extension) : null; } }

        private string _srcFile;
        private string _extension;
        private bool _isChosePicture;

        public frmAddCourse()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            if (CheckEmptyTextBox(txtCourseName)) return;
            if (CheckEmptyTextBox(txtCost)) return;
            if (CheckEmptyTextBox(txtDuration)) return;

            if (!isUpdate)
            {
                var courseName = txtCourseName.Text;
                var cost = Convert.ToDouble(txtCost.Text);
                var duration = Convert.ToInt32(txtDuration.Text);
                var description = txtDescription.Text;
                var picture = Picture;
                CourseDal.AddCourse(courseName, cost, duration, description, picture);

                if (Picture != null)
                {
                    var pathDes = Path.Combine(Helper.RootProject.ProjectPath, @"Image\Course", picture);
                    File.Copy(_srcFile, pathDes, true);
                }

                pictureBox1.Image = Resource.photos_48px;
                pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
            }
            else
            {
                Row["Name"] = txtCourseName.Text;
                Row["Cost"] = txtCost.Text;
                Row["Duration"] = txtDuration.Text;
                Row["Description"] = txtDescription.Text;

                if (Picture != null)
                    Row["Picture"] = Picture;
                CourseDal.UpdateCourse();


                if (Picture != null)
                {
                    var pathDes = Path.Combine(Helper.RootProject.ProjectPath, @"Image\Course", Picture);
                    pictureBox1.Image.Dispose();
                    File.Copy(_srcFile, pathDes, true);
                }

                this.Close();
            }

            ClearTextBox();
        }

        private bool CheckEmptyTextBox(TextBox text)
        {
            if (string.IsNullOrWhiteSpace(text.Text))
            {
                text.Focus();
                return true;
            }
            return false;
        }
        private void ClearTextBox()
        {
            txtCourseName.Text = "";
            txtCost.Text = "";
            txtDuration.Text = "";
            txtDescription.Text = "";
        }
        public void BindingTextBox()
        {
            txtCourseName.Text = Row["Name"].ToString();
            txtCost.Text = Row["Cost"].ToString();
            txtDuration.Text = Row["Duration"].ToString();
            txtDescription.Text = Row["Description"].ToString();

            var picture = Row["Picture"].ToString();
            string path = Path.Combine(Helper.RootProject.ProjectPath, "Image\\Course", picture);
            if (File.Exists(path))
            {
                pictureBox1.Image = Image.FromFile(path);
                pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Browse Portrait Photo";
            openFileDialog.Filter = "Image (*.PNG; *.JPG; *.GIF; *.TIFF)|*.PNG; *.JPG; *.GIF; *.TIFF";
            openFileDialog.FileName = string.Empty;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    _srcFile = openFileDialog.FileName;
                    _extension += Path.GetExtension(_srcFile);

                    //MessageBox.Show(photoFileName);
                    if (pictureBox1.Image != null)
                        pictureBox1.Image.Dispose();

                    pictureBox1.Image = Image.FromFile(_srcFile);
                    pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                    _isChosePicture = true;
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine(Picture);
        }
    }
}
