﻿
namespace SchoolMG.Addition
{
    partial class UserControlClassDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel = new System.Windows.Forms.Panel();
            this.dgvEnrolledStudent = new System.Windows.Forms.DataGridView();
            this.lblTeacherNameShow = new System.Windows.Forms.Label();
            this.lblRoomShow = new System.Windows.Forms.Label();
            this.lblTimeShow = new System.Windows.Forms.Label();
            this.lblCourseNameShow = new System.Windows.Forms.Label();
            this.lblTeacherName = new System.Windows.Forms.Label();
            this.lblRoom = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblCourseName = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lblClassStatusShow = new System.Windows.Forms.Label();
            this.lblClassStatus = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.StudentListbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnEnd = new System.Windows.Forms.Button();
            this.btnActiveClass = new System.Windows.Forms.Button();
            this.EnrollId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EnrollDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditStatus = new System.Windows.Forms.DataGridViewImageColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEnrolledStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StudentListbindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.Controls.Add(this.dgvEnrolledStudent);
            this.panel.Location = new System.Drawing.Point(20, 185);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1311, 390);
            this.panel.TabIndex = 46;
            // 
            // dgvEnrolledStudent
            // 
            this.dgvEnrolledStudent.AllowUserToAddRows = false;
            this.dgvEnrolledStudent.AllowUserToDeleteRows = false;
            this.dgvEnrolledStudent.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.dgvEnrolledStudent.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEnrolledStudent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvEnrolledStudent.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvEnrolledStudent.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.dgvEnrolledStudent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEnrolledStudent.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvEnrolledStudent.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            this.dgvEnrolledStudent.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(41)))), ((int)(((byte)(72)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS Battambang", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(10);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(65)))), ((int)(((byte)(81)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEnrolledStudent.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEnrolledStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEnrolledStudent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EnrollId,
            this.StudentId,
            this.StudentName,
            this.Gender,
            this.Phone,
            this.EnrollDate,
            this.StudentStatus,
            this.EditStatus,
            this.Column1});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Khmer OS Battambang", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEnrolledStudent.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvEnrolledStudent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEnrolledStudent.EnableHeadersVisualStyles = false;
            this.dgvEnrolledStudent.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.dgvEnrolledStudent.Location = new System.Drawing.Point(0, 0);
            this.dgvEnrolledStudent.Name = "dgvEnrolledStudent";
            this.dgvEnrolledStudent.ReadOnly = true;
            this.dgvEnrolledStudent.RowHeadersVisible = false;
            this.dgvEnrolledStudent.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(5, 10, 5, 10);
            this.dgvEnrolledStudent.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvEnrolledStudent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEnrolledStudent.Size = new System.Drawing.Size(1311, 390);
            this.dgvEnrolledStudent.TabIndex = 21;
            this.dgvEnrolledStudent.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEnrolledStudent_CellContentClick);
            // 
            // lblTeacherNameShow
            // 
            this.lblTeacherNameShow.AutoSize = true;
            this.lblTeacherNameShow.Location = new System.Drawing.Point(135, 108);
            this.lblTeacherNameShow.Name = "lblTeacherNameShow";
            this.lblTeacherNameShow.Size = new System.Drawing.Size(109, 27);
            this.lblTeacherNameShow.TabIndex = 45;
            this.lblTeacherNameShow.Text = "Teacher Name";
            // 
            // lblRoomShow
            // 
            this.lblRoomShow.AutoSize = true;
            this.lblRoomShow.Location = new System.Drawing.Point(135, 81);
            this.lblRoomShow.Name = "lblRoomShow";
            this.lblRoomShow.Size = new System.Drawing.Size(51, 27);
            this.lblRoomShow.TabIndex = 44;
            this.lblRoomShow.Text = "Room";
            // 
            // lblTimeShow
            // 
            this.lblTimeShow.AutoSize = true;
            this.lblTimeShow.Location = new System.Drawing.Point(135, 54);
            this.lblTimeShow.Name = "lblTimeShow";
            this.lblTimeShow.Size = new System.Drawing.Size(44, 27);
            this.lblTimeShow.TabIndex = 43;
            this.lblTimeShow.Text = "Time";
            // 
            // lblCourseNameShow
            // 
            this.lblCourseNameShow.AutoSize = true;
            this.lblCourseNameShow.Location = new System.Drawing.Point(135, 27);
            this.lblCourseNameShow.Name = "lblCourseNameShow";
            this.lblCourseNameShow.Size = new System.Drawing.Size(103, 27);
            this.lblCourseNameShow.TabIndex = 42;
            this.lblCourseNameShow.Text = "Course Name";
            // 
            // lblTeacherName
            // 
            this.lblTeacherName.AutoSize = true;
            this.lblTeacherName.Location = new System.Drawing.Point(26, 108);
            this.lblTeacherName.Name = "lblTeacherName";
            this.lblTeacherName.Size = new System.Drawing.Size(72, 27);
            this.lblTeacherName.TabIndex = 41;
            this.lblTeacherName.Text = "គ្រូបង្រៀន";
            // 
            // lblRoom
            // 
            this.lblRoom.AutoSize = true;
            this.lblRoom.Location = new System.Drawing.Point(26, 81);
            this.lblRoom.Name = "lblRoom";
            this.lblRoom.Size = new System.Drawing.Size(42, 27);
            this.lblRoom.TabIndex = 40;
            this.lblRoom.Text = "បន្ទប់";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(26, 54);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(42, 27);
            this.lblTime.TabIndex = 39;
            this.lblTime.Text = "ម៉ោង";
            // 
            // lblCourseName
            // 
            this.lblCourseName.AutoSize = true;
            this.lblCourseName.Location = new System.Drawing.Point(26, 27);
            this.lblCourseName.Name = "lblCourseName";
            this.lblCourseName.Size = new System.Drawing.Size(52, 27);
            this.lblCourseName.TabIndex = 38;
            this.lblCourseName.Text = "មុខវិជ្ជា";
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(82)))), ((int)(((byte)(156)))));
            this.btnEdit.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnEdit.FlatAppearance.BorderSize = 0;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEdit.Location = new System.Drawing.Point(929, 144);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(130, 35);
            this.btnEdit.TabIndex = 47;
            this.btnEdit.Text = "Edit Class";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Visible = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lblClassStatusShow
            // 
            this.lblClassStatusShow.AutoSize = true;
            this.lblClassStatusShow.Location = new System.Drawing.Point(135, 142);
            this.lblClassStatusShow.Name = "lblClassStatusShow";
            this.lblClassStatusShow.Size = new System.Drawing.Size(96, 27);
            this.lblClassStatusShow.TabIndex = 49;
            this.lblClassStatusShow.Text = "Class Status";
            // 
            // lblClassStatus
            // 
            this.lblClassStatus.AutoSize = true;
            this.lblClassStatus.Location = new System.Drawing.Point(26, 142);
            this.lblClassStatus.Name = "lblClassStatus";
            this.lblClassStatus.Size = new System.Drawing.Size(93, 27);
            this.lblClassStatus.TabIndex = 48;
            this.lblClassStatus.Text = "ស្ថានភាពថ្នាក់";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.HeaderText = "EditStatus";
            this.dataGridViewImageColumn1.Image = global::SchoolMG.Resource.edit_20px;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 122;
            // 
            // btnEnd
            // 
            this.btnEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(180)))), ((int)(((byte)(120)))));
            this.btnEnd.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnEnd.FlatAppearance.BorderSize = 0;
            this.btnEnd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEnd.Location = new System.Drawing.Point(1201, 144);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(130, 35);
            this.btnEnd.TabIndex = 50;
            this.btnEnd.Text = "End Class";
            this.btnEnd.UseVisualStyleBackColor = false;
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // btnActiveClass
            // 
            this.btnActiveClass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActiveClass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(118)))), ((int)(((byte)(210)))));
            this.btnActiveClass.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnActiveClass.FlatAppearance.BorderSize = 0;
            this.btnActiveClass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActiveClass.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnActiveClass.Location = new System.Drawing.Point(1065, 144);
            this.btnActiveClass.Name = "btnActiveClass";
            this.btnActiveClass.Size = new System.Drawing.Size(130, 35);
            this.btnActiveClass.TabIndex = 51;
            this.btnActiveClass.Text = "Active Class";
            this.btnActiveClass.UseVisualStyleBackColor = false;
            this.btnActiveClass.Click += new System.EventHandler(this.btnActiveClass_Click);
            // 
            // EnrollId
            // 
            this.EnrollId.DataPropertyName = "Id";
            this.EnrollId.HeaderText = "EnrollId";
            this.EnrollId.Name = "EnrollId";
            this.EnrollId.ReadOnly = true;
            this.EnrollId.Visible = false;
            this.EnrollId.Width = 85;
            // 
            // StudentId
            // 
            this.StudentId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.StudentId.DataPropertyName = "StudentId";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "0000";
            this.StudentId.DefaultCellStyle = dataGridViewCellStyle3;
            this.StudentId.HeaderText = "StudentId";
            this.StudentId.Name = "StudentId";
            this.StudentId.ReadOnly = true;
            this.StudentId.Width = 117;
            // 
            // StudentName
            // 
            this.StudentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.StudentName.DataPropertyName = "StudentName";
            this.StudentName.HeaderText = "Student Name";
            this.StudentName.Name = "StudentName";
            this.StudentName.ReadOnly = true;
            this.StudentName.Width = 233;
            // 
            // Gender
            // 
            this.Gender.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Gender.DataPropertyName = "Gender";
            this.Gender.HeaderText = "Gender";
            this.Gender.Name = "Gender";
            this.Gender.ReadOnly = true;
            this.Gender.Width = 104;
            // 
            // Phone
            // 
            this.Phone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Phone.DataPropertyName = "Phone";
            this.Phone.HeaderText = "Phone";
            this.Phone.Name = "Phone";
            this.Phone.ReadOnly = true;
            this.Phone.Width = 233;
            // 
            // EnrollDate
            // 
            this.EnrollDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EnrollDate.DataPropertyName = "EnrollDate";
            this.EnrollDate.HeaderText = "EnrollDate";
            this.EnrollDate.Name = "EnrollDate";
            this.EnrollDate.ReadOnly = true;
            this.EnrollDate.Width = 123;
            // 
            // StudentStatus
            // 
            this.StudentStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.StudentStatus.DataPropertyName = "StudentStatus";
            this.StudentStatus.HeaderText = "StudentStatus";
            this.StudentStatus.Name = "StudentStatus";
            this.StudentStatus.ReadOnly = true;
            this.StudentStatus.Width = 147;
            // 
            // EditStatus
            // 
            this.EditStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.EditStatus.HeaderText = "EditStatus";
            this.EditStatus.Image = global::SchoolMG.Resource.edit_20px;
            this.EditStatus.Name = "EditStatus";
            this.EditStatus.ReadOnly = true;
            this.EditStatus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EditStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EditStatus.Width = 122;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // UserControlClassDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnActiveClass);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.lblClassStatusShow);
            this.Controls.Add(this.lblClassStatus);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.lblTeacherNameShow);
            this.Controls.Add(this.lblRoomShow);
            this.Controls.Add(this.lblTimeShow);
            this.Controls.Add(this.lblCourseNameShow);
            this.Controls.Add(this.lblTeacherName);
            this.Controls.Add(this.lblRoom);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblCourseName);
            this.Font = new System.Drawing.Font("Khmer OS Battambang", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UserControlClassDetail";
            this.Size = new System.Drawing.Size(1350, 615);
            this.Load += new System.EventHandler(this.UserControlClassDetail_Load);
            this.panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEnrolledStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StudentListbindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Panel panel;
        public System.Windows.Forms.DataGridView dgvEnrolledStudent;
        public System.Windows.Forms.Label lblTeacherNameShow;
        public System.Windows.Forms.Label lblRoomShow;
        public System.Windows.Forms.Label lblTimeShow;
        public System.Windows.Forms.Label lblCourseNameShow;
        private System.Windows.Forms.Label lblTeacherName;
        private System.Windows.Forms.Label lblRoom;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblCourseName;
        private System.Windows.Forms.BindingSource StudentListbindingSource;
        private System.Windows.Forms.Button btnEdit;
        public System.Windows.Forms.Label lblClassStatusShow;
        private System.Windows.Forms.Label lblClassStatus;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.Button btnActiveClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnrollId;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentId;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn EnrollDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentStatus;
        private System.Windows.Forms.DataGridViewImageColumn EditStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}
