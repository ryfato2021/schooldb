﻿using SchoolMG.DAL;
using SchoolMG.Student;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Addition
{
    public partial class UserControlClassDetail : UserControl
    {
        public Home Home = null;
        public int ClassId { get; set; }
        public string CourseName { get; set; }
        public PaymentDal PaymentDal = new PaymentDal();
        public UserControlClassDetail()
        {
            InitializeComponent();
            dgvEnrolledStudent.AutoGenerateColumns = false;

            
        }

        public void ShowStudentList()
        {
            var enrollStudent = PaymentDal.GetEnrolledStudentByClass(ClassId);

            dgvEnrolledStudent.DataSource = enrollStudent;

            if (enrollStudent.Rows.Count <= 0)
            {
                panel.Controls.Clear();
                panel.Controls.Add(new Label
                {
                    Text = "មិនមានសិស្ស សិក្សាថ្នាក់នេះទេ",
                    AutoSize = false,
                    Dock = DockStyle.Fill,
                    Font = new Font("Khmer Os Battambang", 18),
                    ForeColor = Color.FromArgb(127, 127, 127, 127),
                    TextAlign = ContentAlignment.TopCenter
                }); ;
            }
            //
            //
            //
            ControlButton();

        }

        void ControlButton()
        {

            //
            // Validate btnActiveClass
            //
            if (lblClassStatusShow.Text.Trim() == Enumerator.ClassState.Pedding.ToString())
            {
                btnActiveClass.Enabled = true;
            }
            else btnActiveClass.Enabled = false;

            //
            // Validate btnEndClass
            //
            if (lblClassStatusShow.Text.Trim() == Enumerator.ClassState.Active.ToString())
            {
                btnEnd.Enabled = true;
            }
            else btnEnd.Enabled = false;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }

        private void btnEnd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure to End This Class??", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            const int endClass = 3;
            var classStatus = Helper.DbHelper.GetScalarValue($"Select Status From Classes Where Id = {ClassId}");
            Helper.DbHelper.UpdateData($"exec dbo.UpdateClassStatus {ClassId}, {endClass}");
            lblClassStatusShow.Text = Enumerator.ClassState.Completed.ToString();
            ShowStudentList();
        }

        private void btnActiveClass_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure to Set This Class Active (Studying Mode)??", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;

            var classStatus = Helper.DbHelper.GetScalarValue($"Select Status From Classes Where Id = {ClassId}");
            Helper.DbHelper.UpdateData($"exec dbo.UpdateClassStatus {ClassId}, {Enumerator.ClassState.Active:d}");
            lblClassStatusShow.Text = Enumerator.ClassState.Active.ToString();
            ShowStudentList();
        }

        private void UserControlClassDetail_Load(object sender, EventArgs e)
        {

        }

        private void dgvEnrolledStudent_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dgvEnrolledStudent.CurrentCell.OwningColumn.Name == "EditStatus")
            {
                var currentRow = dgvEnrolledStudent.Rows[e.RowIndex];
                frmEditStudentStatus status = new frmEditStudentStatus();
                status.CurrentStatus = (string)currentRow.Cells["StudentStatus"].Value;
                var state = status.ShowEditMode();

                if (state == -1) return;

                var enrollId = (int)currentRow.Cells["EnrollId"].Value;
                var studentId = (int)currentRow.Cells["StudentId"].Value;

                var tableEnrollment = (DataTable)dgvEnrolledStudent.DataSource;
                tableEnrollment.Columns["Studentstatus"].ReadOnly = false;

                var row = tableEnrollment.Rows.Find(new object[]{ enrollId, studentId });
                row["StudentStatus"] = (Enumerator.StudentStatus)state;

                PaymentDal.UpdateStudentStatus(state, enrollId);
            }
        }
    }
}
