﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolMG.Helper;
using SchoolMG.Enumerator;

namespace SchoolMG.Addition
{
    public partial class NewClass : Form
    {
        public int Course { get; set; }
        public Turn Turn { get; set; }
        public int Time{ get; set; }
        public int Room { get; set; }
        public int Teacher { get; set; }

        public NewClass()
        {
            InitializeComponent();
            
            initialData();

            cbCourse.SelectedIndexChanged += CbCourse_SelectedIndexChanged;

            cbTurn.SelectedIndexChanged += CbTurn_SelectedIndexChanged;

            cbTime.SelectedIndexChanged += CbTime_SelectedIndexChanged;

            cbRoom.SelectedIndexChanged += CbRoom_SelectedIndexChanged;

            cbTeacher.SelectedIndexChanged += CbTeacher_SelectedIndexChanged;


            checkTextFill();
            
        }

        void checkTextFill()
        {
            bool enable = (cbCourse.SelectedIndex != 0
                && cbTime.SelectedIndex != 0
                //&& cbRoom.SelectedIndex !=0
                //&& cbTeacher.SelectedIndex != 0
                );

            btnCreate.Enabled = enable;
        }

        void initialWeek()
        {
            cbTurn.DataSource = Enum.GetValues(typeof(Turn));
            cbTurn.DisplayMember = "Week";
            Turn = (Turn)Enum.Parse(typeof(Turn), cbTurn.Text);

            //Console.WriteLine("Turn: " + Turn);
            //Console.WriteLine( SchoolMG.Helper.DbHelper.IsConnected);
        }

        void initialData()
        {
            // Turn of Week
            //
            initialWeek();
            //
            // Get All Courses
            //
            initialCourse();

            //
            // Get  Available Time
            //
            initialAvailabelTime();
            //
            // Get Availble Room
            //
            initialAvailableRoom();
            //
            // Get Availble teacher
            //
            initialAvailableTeacher();
        }
        public async void initialCourse()
        {
            DataTable dtCourse =  DbHelper.GetData("SELECT Id, Name From GetAllCourses");

            DataRow row = dtCourse.NewRow();
            row[0] = -1;
            row[1] = "មុខវិជ្ជា";
            dtCourse.Rows.InsertAt(row, 0);
            cbCourse.DataSource = dtCourse;
            cbCourse.DisplayMember = "Name";
            cbCourse.ValueMember = "Id";
            cbCourse.MaxDropDownItems = 5;

            Course = (int)cbCourse.SelectedValue;

        }
        async void initialAvailabelTime()
        {
            DataTable dtTime =  DbHelper .GetData(string.Format("GetAvailableTime {0}", Course));
            DataRow row = dtTime.NewRow();
            row[0] = -1;
            row[1] = "ម៉ោង";
            dtTime.Rows.InsertAt(row, 0);
            cbTime.DataSource = dtTime;
            cbTime.DisplayMember = "Time";
            cbTime.ValueMember = "Id";

            Time = (int)cbTime.SelectedValue;

        }
        async void initialAvailableRoom()
        {
            DataTable dtRoom =  DbHelper.GetData($"exec dbo.SelectFreeRoomByTime {Turn:D}, {Time}");
            DataRow row = dtRoom.NewRow();
            row[0] = -1;
            row[1] = "បន្ទប់";
            dtRoom.Rows.InsertAt(row, 0);
            cbRoom.DataSource = dtRoom;
            cbRoom.DisplayMember = "RoomName";
            cbRoom.ValueMember = "RoomId";

            Room = (int)cbRoom.SelectedValue;

            
        }

        async void initialAvailableTeacher()
        {
            DataTable dtTeahcer =  DbHelper.GetData(string.Format("dbo.SelectFreeTeacherByTime {0:D}, {1}", Turn, Time));
            DataRow row = dtTeahcer.NewRow();
            row[0] = -1;
            row[1] = "គ្រូ";
            dtTeahcer.Rows.InsertAt(row, 0);
            cbTeacher.DataSource = dtTeahcer;
            cbTeacher.DisplayMember = "TeacherName";
            cbTeacher.ValueMember = "TeacherId";

            Teacher = (int)cbTeacher.SelectedValue;
        }

        private void CbTeacher_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Teacher = (int)cbTeacher.SelectedValue;
            Console.WriteLine("Teacher: " + Teacher);
            checkTextFill();
        }

        private void CbRoom_SelectedIndexChanged(object sender, EventArgs e)
        {

            Room = (int)cbRoom.SelectedValue;
            Console.WriteLine("Room: " + Room);
            initialAvailableTeacher();
            checkTextFill();
        }

        private void CbTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            Time = (int)cbTime.SelectedValue;
            Console.WriteLine("Time: {0}", Time);
            initialAvailableRoom();
            checkTextFill();
        }

        private void CbTurn_SelectedIndexChanged(object sender, EventArgs e)
        {
            Turn = (Turn)Enum.Parse(typeof(Turn), cbTurn.Text);
            Console.WriteLine("Turn: {0:D}", Turn);
            initialAvailabelTime();
            checkTextFill();
        }

        private void CbCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            Course = (int)cbCourse.SelectedValue;
            Console.WriteLine("Course: " + Course);
            initialAvailabelTime();
            checkTextFill();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void refresh()
        {
            initialCourse();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {

        }
    }
}
