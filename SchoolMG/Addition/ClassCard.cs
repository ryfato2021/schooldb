﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SchoolMG.Addition
{
    public partial class ClassCard : UserControl
    {
        public int ClassId { get; set; }
        public string CourseName { get; set; }
        public string Time { get; set; }
        public Enumerator.Turn Turn { get; set; }
        public string RoomName { get; set; }
        public string TeacherName { get; set; }
        public Enumerator.ClassState ClassStatus { get; set; }
        public int StudentAmount { get; set; }
        public string Picture { get; set; }

        public DataRow Row;
        public ClassCard()
        {
            InitializeComponent();
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
           // MessageBox.Show($"ClassId: {ClassId}");
        }

        public void binding()
        {
            this.lblCourse.Text = CourseName;
            this.lblRoom.Text = RoomName;
            this.lblTime.Text = Time;
            this.lblTurn.Text = Turn.ToString();
            this.lblTeacher.Text = TeacherName;
            this.lblStatus.Text = ClassStatus.ToString();

            this.lblCountStudent.Text = StudentAmount.ToString("0 នាក់");

            var path = Path.Combine(Helper.RootProject.ProjectPath, "Image\\Course", Picture == null ? "" : Picture);
            if (File.Exists(path))
            {
                this.pictureBox1.Image = Image.FromFile(path);
                this.pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            }
            else
            {
                this.pictureBox1.Image = Resource.photos_48px;
                this.pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
