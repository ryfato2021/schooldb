﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolMG.Addition
{
    public partial class ClassInfo : UserControl
    {
        public int Fetch { get; set; }
        public Enumerator.ClassState? ClassState { get; set; }
        public Addition.ClassCard[] classCards;
        public Addition.NewClass NewClass = new Addition.NewClass();
        public DataTable ClassTable { get; private set; }

        public Home Home = null;
        async Task<DataTable> FetchData()
        {
            string str = $"SELECT * FROM VGetAllClasses ";
            if(cbClassState.SelectedIndex > 0)
            {
                str += $"Where ClassStatus = { ClassState:D} ";

                if (cbTurn.SelectedIndex > 0)
                {
                    str += $" And Turn={cbTurn.SelectedValue}";
                }
            }
            else
            {
                if(cbTurn.SelectedIndex >0)
                str += $"Where Turn = {cbTurn.SelectedValue}";
            }
            
            str += "Order by ClassId Desc";
            var data =  Helper.DbHelper.GetData(str);

            return data;
        }

        void StartPagination()
        {
            FetchStart = 0;
            FetchAmount = 12;
            Page = (int)Math.Ceiling(ClassTable.Rows.Count / (double)FetchAmount);
            Next = 1;
            setPage();

            if (Page <= 1)
            {
                btnForward.Enabled = false;
            }
            else btnForward.Enabled = true;
        }
        private int FetchStart;
        private int FetchAmount;
        private int Page;
        private int Next ;

        //private DataTable dtClass = new DataTable();

        public ClassInfo()
        {
            InitializeComponent();
            //initialClassCard(12);

            this.Load += ClassInfo_Load;

            var state = Enum.GetNames(typeof(Enumerator.ClassState));
            List<string> states = new List<string>(state);

            states.Insert(0, "All");


            
            cbClassState.DataSource = states;
            cbClassState.SelectedIndex = 0;
            Enumerator.ClassState classState;
            Enum.TryParse<Enumerator.ClassState>(cbClassState.Text, out classState);
            ClassState = classState;

            //
            // Turn
            //
            DataTable tableTurn = new DataTable("Turn");
            tableTurn.Columns.Add("TurnId", typeof(int));
            tableTurn.Columns.Add("Turn", typeof(string));
            tableTurn.Rows.Add(0, "Weekday");
            tableTurn.Rows.Add(1, "Weekend");
            var row = tableTurn.NewRow();
            row["TurnId"] = -1;
            row["Turn"] = "All";
            tableTurn.Rows.InsertAt(row, 0);
            cbTurn.DataSource = tableTurn;
            cbTurn.DisplayMember = "Turn";
            cbTurn.ValueMember = "TurnId";
            cbTurn.SelectedIndex = 0;

            NewClass.btnCreate.Click += BtnCreate_Click;
        }

        private async void ClassInfo_Load(object sender, EventArgs e)
        {
            ClassTable = await FetchData();
            setPage();
            initialClassCard();
        }

        private void BtnCreate_Click(object sender, EventArgs e)
        {
            string teacher = NewClass.Teacher != -1 ? NewClass.Teacher.ToString() : "null";
            string room = NewClass.Room != -1 ? NewClass.Room.ToString() : "null";
            string sql = $"exec dbo.CreateClass {NewClass.Course}, {NewClass.Time}, {NewClass.Turn:D}, {room}, {teacher}";
            Console.WriteLine("Sql: " + sql);
            Helper.DbHelper.InsertData(sql);

            NewClass.initialCourse();
            //setClassCard($"GetAllClasses {ClassState:d}");
        }

        void initialClassCard()
        {
            StartPagination();
            btnBack.Enabled = false;
            LoadClassCards();

            Console.WriteLine("Count: {0}", (Enumerator.ClassState)1);

        }

        private void cbClassState_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enumerator.ClassState? state;
            if (cbClassState.SelectedIndex == 0)
                state = null;
            else
                state = (Enumerator.ClassState)Enum.Parse(typeof(Enumerator.ClassState), cbClassState.Text);

            if (state == ClassState) return;

            ClassState = state;

            //Console.WriteLine("Class State: {0:d}->{0}", ClassState);
            //initialClassCard(Fetch);
        }

        private async void LoadClassCards()
        {
            if (this.flowLayoutPanel1.Controls.Count > 0)
            {
                this.flowLayoutPanel1.Controls.Clear();
            }
            var endrow = FetchStart + FetchAmount;
            var classCards = new List<ClassCard>() ;
            for (int i = FetchStart; i < endrow; i++)
            {
                if (i == ClassTable.Rows.Count) break;
                
                var index = i - FetchStart;
                var classCard = new ClassCard
                {
                    Location = new System.Drawing.Point(0, 0),
                    ClassId = ClassTable.Rows[i].Field<int>("ClassId"),
                    CourseName = ClassTable.Rows[i].Field<string>("CourseName"),
                    RoomName = ClassTable.Rows[i].Field<string>("RoomName"),
                    Time = ClassTable.Rows[i].Field<string>("Time"),
                    Turn = (Enumerator.Turn)ClassTable.Rows[i].Field<int>("Turn"),
                    ClassStatus = (Enumerator.ClassState)ClassTable.Rows[i].Field<int>("ClassStatus"),
                    TeacherName = ClassTable.Rows[i].Field<string>("TeacherName"),
                    StudentAmount = ClassTable.Rows[i].Field<int>("CountStudent"),
                    Picture = ClassTable.Rows[i]["Picture"].ToString()
                };
                classCard.binding();
                classCard.btnDetail.Click += BtnDetail_Click;
                classCard.pictureBox1.Click += PictureBox1_Click;

                classCards.Add(classCard);
            }

            txtCourseAmount.Text = ClassTable.Rows.Count.ToString();

            if (classCards.Count == 0)
            {
                this.flowLayoutPanel1.Controls.Add(new Label()
                {
                    Text = "មិនមានទិន្នន័យ",
                    Font = new Font("Khmer Os Battambang", 18),
                    ForeColor = Color.FromArgb(128,128,128,100),
                    AutoSize=true,
                    Anchor = AnchorStyles.None
                });
                return;
            }
            this.flowLayoutPanel1.Controls.AddRange(classCards.ToArray());
            
            
            
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            if (Home != null)
            {
                var classCard = (ClassCard)((PictureBox)sender).Parent.Parent;
                UserControlClassDetail detail = new UserControlClassDetail();
                detail.lblCourseNameShow.Text = classCard.CourseName;
                detail.lblTimeShow.Text = classCard.Time;
                detail.lblRoomShow.Text = classCard.RoomName;
                detail.lblTeacherNameShow.Text = classCard.TeacherName;
                detail.lblClassStatusShow.Text = classCard.ClassStatus.ToString();

                detail.ClassId = classCard.ClassId;
                detail.Home = Home;
                detail.ShowStudentList();

                Home.ShowUp(detail);

            }
            else
            {
                MessageBox.Show("I hate You");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDetail_Click(object sender, EventArgs e)
        {
            if(Home != null)
            {
                var classCard = (ClassCard)((Button)sender).Parent;
                UserControlClassDetail detail = new UserControlClassDetail();
                detail.lblCourseNameShow.Text = classCard.CourseName;
                detail.lblTimeShow.Text = classCard.Time;
                detail.lblRoomShow.Text = classCard.RoomName;
                detail.lblTeacherNameShow.Text = classCard.TeacherName;
                detail.lblClassStatusShow.Text = classCard.ClassStatus.ToString();

                detail.ClassId = classCard.ClassId;
                detail.Home = Home;
                detail.ShowStudentList();

                Home.ShowUp(detail);
                
            }
            else
            {
                MessageBox.Show("I hate You");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var search = txtSearch.Text;
            
            if (string.IsNullOrWhiteSpace(search))
            {
                txtSearch.Focus();
                return;
            }

            string searchExpression = $"Convert(ClassId, 'System.String') = '{search}' " +
            $"Or CourseName like '%{search}%' Or RoomName like '%{search}%' Or TeacherName like '%{search}%'" +
            $"Or Time like '%{search}%'";

            var searchData = new DataView(ClassTable);
            searchData.RowFilter = searchExpression;

            var BackUpTable = ClassTable;
            ClassTable = searchData.ToTable();
            initialClassCard();

            ClassTable = BackUpTable;

        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter) btnSearch_Click(this, null);
        }

        private void btnAddNewClass_Click(object sender, EventArgs e)
        {
            NewClass.StartPosition = FormStartPosition.CenterScreen;
            NewClass.ShowDialog();
        }

        private async void btnRefresh_Click(object sender, EventArgs e)
        {
            cbClassState.SelectedIndex = 0;
            cbTurn.SelectedIndex = 0;

            ClassTable = await FetchData();
            //setPage();
            initialClassCard();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (Next -1 == 1) btnBack.Enabled = false;

            if (Next > 1)
            {
                Next--;
                FetchStart -= FetchAmount;
                LoadClassCards();

                setPage();
                btnForward.Enabled = true;
            }

        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            if (Next >= Page - 1) btnForward.Enabled = false;
            Next++;
            FetchStart += FetchAmount;
            LoadClassCards();

            setPage();
            btnBack.Enabled = true;
        }
        private void setPage()
        {
            this.lblPages.Text = $"{Next} of {Page}";
        }

        private async void btnLoad_Click(object sender, EventArgs e)
        {
            ClassTable = await FetchData();
            //setPage();
            initialClassCard();
        }
    }
    public class FetchAmount
    {
        public int Value { get; set; }
    }
}
