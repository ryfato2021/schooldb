﻿using SchoolMG.Payment;
using SchoolMG.Register;
using SchoolMG.Student;
using System;
using System.Collections;
using System.Windows.Forms;
namespace SchoolMG
{

    public partial class Home : Form
    {
        public frmLogIn frmLogIn;
        public StartUp Start { get; set; }
        public StudentInfo studentInfo { get; set; }
        public StudentDetail StudentDetail { get; set; }
        public StudentPayment StudentPayment { get; set; }
        public Addition.ClassInfo Class { get; set; }
        public DAL.StudentDal StudentDal { get; set; }

        public Addition.NewClass NewClass { get; set; }

        public Stack MainStack = new Stack();
        public int UserId { get; private set; }

        public Home()
        {
            InitializeComponent();

            //initializeStack();

            Start = new StartUp { Dock = DockStyle.Fill };
            Start.Home = this;

            studentInfo = new StudentInfo() { Dock = DockStyle.Fill };

            Start.StudentDal = studentInfo.studentDal;

            studentInfo.dgvStudentInfo.CellContentClick += DgvStudentInfo_CellContentClick;

            Start.btnStudentInfo.Click += BtnStudentInfo_Click;
            Start.btnClass.Click += BtnClass_Click;

            NewClass = new Addition.NewClass();
            NewClass.btnCreate.Click += BtnCreate_Click;

            //StudentDetail = new StudentDetail();

            StudentPayment = new StudentPayment
            {
                Dock = DockStyle.Fill,
            };

            //
            // ClassInfo
            //
            Class = new Addition.ClassInfo()
            {
                Dock = DockStyle.Fill,
                AutoSize = true,
                //Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top
            };
            Class.txtSearch.KeyUp += TxtSearch_KeyUp;
        }

        

        //
        // Event CellContentClick on DataGridView of StudentInfo
        //
        private void DgvStudentInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var current = studentInfo.dgvStudentInfo.CurrentCell;
            if (current is DataGridViewImageCell && e.RowIndex != -1)
            {
                Console.WriteLine("IndexRow: {0}", e.RowIndex);

                var cell = studentInfo.dgvStudentInfo.Rows[e.RowIndex].Cells;

                var studentId = cell["Id"].Value;

                //studentInfo.StudentDetail = new StudentDetail();
                StudentDetail = new StudentDetail();
                StudentDetail.StudentDal = studentInfo.studentDal;
                StudentDetail.StudentId = (int)studentId;

                StudentDetail.ShowStudentDetails(studentId);
                StudentDetail.ShowStudentStudayInfo();
                StudentDetail.AutoSize = true;
                StudentDetail.Dock = DockStyle.Fill;

                ShowUp(StudentDetail);
            }
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            var tmpClass = Class;
            if (string.IsNullOrWhiteSpace(((TextBox)sender).Text)) return;

            if (e.KeyCode == Keys.Enter)
            {
                this.MainStack.Push(tmpClass);
                Class = new Addition.ClassInfo() { Dock = DockStyle.Fill };
                Console.WriteLine("Stack: {0}", MainStack.Count);
            }
        }

        private void BtnClass_Click(object sender, EventArgs e)
        {
            Class = new Addition.ClassInfo() { Dock = DockStyle.Fill };
            Class.Home = this;
            ShowUp(Class);
        }

        void initializeStack()
        {
            if (MainStack.Count <= 1)
            {
                btnBack.Enabled = false;
                return;
            }
        }

        void Backward()
        {
            this.pnlMian.Controls.Clear();
            this.MainStack.Pop();
            this.pnlMian.Controls.Add((Control)MainStack.Peek());

            if (MainStack.Count <= 1)
            {
                btnBack.Enabled = false;
            }
            Console.WriteLine("MainStack Item Count: {0}", MainStack.Count);
        }


        private void BtnStudentInfo_Click(object sender, EventArgs e)
        {
            //StudentInfo = new StudentInfo { Dock = DockStyle.Fill };
            studentInfo.PopulateStudent();
            ShowUp(studentInfo);
        }

        private void classToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Class is Clicked!!!");
        }

        private void Home_Load(object sender, EventArgs e)
        {
            //Get UserId
            UserId = LoginPop();
            
            PopStart();
            
        }

        public void ShowUp(UserControl userControl)
        {
            userControl.Dock = DockStyle.Fill;
            userControl.AutoSize = true;
            this.pnlMian.Controls.Clear();
            this.pnlMian.Controls.Add(userControl);
            this.MainStack.Push(userControl);
            this.btnBack.Enabled = true;

            Console.WriteLine("Count: {0}", MainStack.Count);
        }

        private void PopStart()
        {
            if (this.pnlMian.Controls.Contains(Start)) { return; };

            this.pnlMian.Controls.Clear();
            this.pnlMian.Controls.Add(Start);
            this.MainStack.Clear();
            this.MainStack.Push(Start);
            this.btnBack.Enabled = false;
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            PopStart();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Backward();
        }


        private void Home_MaximumSizeChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Hey you have changed the size");
        }

        private void Home_MaximizedBoundsChanged(object sender, EventArgs e)
        {

        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //get UserId
            UserId = LoginPop();
        }

        private int LoginPop()
        {
            this.Visible = false;
            frmLogIn = new frmLogIn();

            var userId = frmLogIn.ShowLoginForm();

            if (userId <= 0)
            {
                this.Close();
            }
            else
            {
                this.Visible = true;
                this.PopStart();
            }
            return userId;
        }

        private void Home_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you Sure To Close the Programm", "Confirming", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void StudentSellRptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Report.StudentReport report = new Report.StudentReport();
            report.Home = this;
            this.ShowUp(report);
        }

        private void របយករណលកToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Report.SellReport report = new Report.SellReport();
            report.Home = this;
            this.ShowUp(report);
        }

        private void closeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void newStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var RegisterWizard = new Wizard();
            RegisterWizard.StudentDal = studentInfo.studentDal;
            RegisterWizard.ShowDialog();
        }

        private void listStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            studentInfo.PopulateStudent();
            ShowUp(studentInfo);
        }

        private void newClassToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewClass.StartPosition = FormStartPosition.CenterScreen;
            var res = NewClass.ShowDialog();

            if (res == DialogResult.Cancel) return;

            Class = new Addition.ClassInfo() { Dock = DockStyle.Fill };
            ShowUp(Class);
        }

        private void BtnCreate_Click(object sender, EventArgs e)
        {
            string teacher = NewClass.Teacher != -1 ? NewClass.Teacher.ToString() : "null";
            string room = NewClass.Room != -1 ? NewClass.Room.ToString() : "null";
            string sql = $"exec dbo.CreateClass {NewClass.Course}, {NewClass.Time}, {NewClass.Turn:D}, {room}, {teacher}";
            Console.WriteLine("Sql: " + sql);
            Helper.DbHelper.InsertData(sql);

            NewClass.initialCourse();
        }

        private void ClassDetailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Class = new Addition.ClassInfo() { Dock = DockStyle.Fill };
            Class.Home = this;
            ShowUp(Class);
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
