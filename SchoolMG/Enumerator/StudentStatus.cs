﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.Enumerator
{
    public enum StudentStatus
    {
        Pendding =0,
        Studying = 1,
        Completed = 2,
        Stopped = 3,
        Suspended = 4
    }
}
