﻿
namespace SchoolMG.OwingStudent
{
    partial class OwingStudentDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OwingStudentDetail));
            this.dgvOwingDetail = new System.Windows.Forms.DataGridView();
            this.btnSetPaybackDate = new System.Windows.Forms.Button();
            this.txtPayback = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCourse = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotolPrice = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPaid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRemain = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblHeader = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.picIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOwingDetail)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvOwingDetail
            // 
            this.dgvOwingDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOwingDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOwingDetail.Location = new System.Drawing.Point(17, 353);
            this.dgvOwingDetail.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.dgvOwingDetail.Name = "dgvOwingDetail";
            this.dgvOwingDetail.Size = new System.Drawing.Size(830, 223);
            this.dgvOwingDetail.TabIndex = 0;
            // 
            // btnSetPaybackDate
            // 
            this.btnSetPaybackDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSetPaybackDate.Location = new System.Drawing.Point(519, 309);
            this.btnSetPaybackDate.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSetPaybackDate.Name = "btnSetPaybackDate";
            this.btnSetPaybackDate.Size = new System.Drawing.Size(223, 36);
            this.btnSetPaybackDate.TabIndex = 1;
            this.btnSetPaybackDate.Text = "កំណត់កាលបរិច្ឆេទសងប្រាក់បន្ទាប់";
            this.btnSetPaybackDate.UseVisualStyleBackColor = true;
            // 
            // txtPayback
            // 
            this.txtPayback.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPayback.Location = new System.Drawing.Point(750, 310);
            this.txtPayback.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.txtPayback.Name = "txtPayback";
            this.txtPayback.Size = new System.Drawing.Size(97, 36);
            this.txtPayback.TabIndex = 1;
            this.txtPayback.Text = "សងប្រាក់";
            this.txtPayback.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.txtName);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.txtPhone);
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.txtCourse);
            this.flowLayoutPanel1.Controls.Add(this.label5);
            this.flowLayoutPanel1.Controls.Add(this.txtPrice);
            this.flowLayoutPanel1.Controls.Add(this.label6);
            this.flowLayoutPanel1.Controls.Add(this.txtDiscount);
            this.flowLayoutPanel1.Controls.Add(this.label7);
            this.flowLayoutPanel1.Controls.Add(this.txtTotolPrice);
            this.flowLayoutPanel1.Controls.Add(this.label4);
            this.flowLayoutPanel1.Controls.Add(this.txtPaid);
            this.flowLayoutPanel1.Controls.Add(this.label8);
            this.flowLayoutPanel1.Controls.Add(this.txtRemain);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(40, 92);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(787, 200);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "ឈ្មោះសិស្ស";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(127, 3);
            this.txtName.Margin = new System.Windows.Forms.Padding(7, 3, 3, 7);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(259, 35);
            this.txtName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(393, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 35);
            this.label2.TabIndex = 2;
            this.label2.Text = "លេខទូរសព្ទ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(516, 3);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(7, 3, 3, 7);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(259, 35);
            this.txtPhone.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 45);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 35);
            this.label3.TabIndex = 4;
            this.label3.Text = "មុខវិជ្ជា";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCourse
            // 
            this.txtCourse.Location = new System.Drawing.Point(127, 48);
            this.txtCourse.Margin = new System.Windows.Forms.Padding(7, 3, 3, 7);
            this.txtCourse.Name = "txtCourse";
            this.txtCourse.Size = new System.Drawing.Size(259, 35);
            this.txtCourse.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(393, 45);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 35);
            this.label5.TabIndex = 8;
            this.label5.Text = "តម្លៃមុខវិជ្ជា";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(516, 48);
            this.txtPrice.Margin = new System.Windows.Forms.Padding(7, 3, 3, 7);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(259, 35);
            this.txtPrice.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(4, 90);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 35);
            this.label6.TabIndex = 10;
            this.label6.Text = "បញ្ចុះតម្លៃ(%)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDiscount
            // 
            this.txtDiscount.Location = new System.Drawing.Point(127, 93);
            this.txtDiscount.Margin = new System.Windows.Forms.Padding(7, 3, 3, 7);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(259, 35);
            this.txtDiscount.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(393, 90);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 35);
            this.label7.TabIndex = 12;
            this.label7.Text = "តម្លៃសរុប";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTotolPrice
            // 
            this.txtTotolPrice.Location = new System.Drawing.Point(516, 93);
            this.txtTotolPrice.Margin = new System.Windows.Forms.Padding(7, 3, 3, 7);
            this.txtTotolPrice.Name = "txtTotolPrice";
            this.txtTotolPrice.Size = new System.Drawing.Size(259, 35);
            this.txtTotolPrice.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 135);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 35);
            this.label4.TabIndex = 14;
            this.label4.Text = "បានបង់";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPaid
            // 
            this.txtPaid.Location = new System.Drawing.Point(127, 138);
            this.txtPaid.Margin = new System.Windows.Forms.Padding(7, 3, 3, 7);
            this.txtPaid.Name = "txtPaid";
            this.txtPaid.Size = new System.Drawing.Size(259, 35);
            this.txtPaid.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(393, 135);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 35);
            this.label8.TabIndex = 16;
            this.label8.Text = "នៅជំពាក់";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtRemain
            // 
            this.txtRemain.Location = new System.Drawing.Point(516, 138);
            this.txtRemain.Margin = new System.Windows.Forms.Padding(7, 3, 3, 7);
            this.txtRemain.Name = "txtRemain";
            this.txtRemain.Size = new System.Drawing.Size(259, 35);
            this.txtRemain.TabIndex = 17;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(65)))), ((int)(((byte)(81)))));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.777778F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.22222F));
            this.tableLayoutPanel2.Controls.Add(this.lblHeader, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(865, 74);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // lblHeader
            // 
            this.lblHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(65)))), ((int)(((byte)(81)))));
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeader.Font = new System.Drawing.Font("Khmer M1", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.ForeColor = System.Drawing.Color.White;
            this.lblHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblHeader.Location = new System.Drawing.Point(78, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblHeader.Size = new System.Drawing.Size(784, 74);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "ព័ត៌មានសងប្រាក់";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.picIcon);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(69, 68);
            this.panel1.TabIndex = 1;
            // 
            // picIcon
            // 
            this.picIcon.ErrorImage = ((System.Drawing.Image)(resources.GetObject("picIcon.ErrorImage")));
            this.picIcon.Image = ((System.Drawing.Image)(resources.GetObject("picIcon.Image")));
            this.picIcon.Location = new System.Drawing.Point(18, 11);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new System.Drawing.Size(48, 48);
            this.picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picIcon.TabIndex = 1;
            this.picIcon.TabStop = false;
            // 
            // OwingStudentDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 594);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.txtPayback);
            this.Controls.Add(this.btnSetPaybackDate);
            this.Controls.Add(this.dgvOwingDetail);
            this.Font = new System.Drawing.Font("Khmer OS Siemreap", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "OwingStudentDetail";
            this.Text = "OwingStudentDetail";
            ((System.ComponentModel.ISupportInitialize)(this.dgvOwingDetail)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvOwingDetail;
        private System.Windows.Forms.Button btnSetPaybackDate;
        private System.Windows.Forms.Button txtPayback;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCourse;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTotolPrice;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picIcon;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPaid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtRemain;
    }
}