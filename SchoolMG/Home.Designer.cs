﻿
namespace SchoolMG
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.classToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClassDetailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.courseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newCourseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listCourseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newRoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listRoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTeacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listTeacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.របយករណលកToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StudentSellRptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlMian = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Khmer OS Battambang", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.classToolStripMenuItem,
            this.studentToolStripMenuItem,
            this.reportToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(6, 4, 0, 4);
            this.menuStrip1.Size = new System.Drawing.Size(1264, 39);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.aboutToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.closeToolStripMenuItem1});
            this.fileToolStripMenuItem.Image = global::SchoolMG.Resource.application_window_48px;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(75, 31);
            this.fileToolStripMenuItem.Text = "កម្មវិធី";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newUserToolStripMenuItem,
            this.allUsersToolStripMenuItem});
            this.toolStripMenuItem1.Image = global::SchoolMG.Resource.user_48px;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 32);
            this.toolStripMenuItem1.Text = "Users";
            // 
            // newUserToolStripMenuItem
            // 
            this.newUserToolStripMenuItem.Name = "newUserToolStripMenuItem";
            this.newUserToolStripMenuItem.Size = new System.Drawing.Size(150, 32);
            this.newUserToolStripMenuItem.Text = "New User";
            // 
            // allUsersToolStripMenuItem
            // 
            this.allUsersToolStripMenuItem.Name = "allUsersToolStripMenuItem";
            this.allUsersToolStripMenuItem.Size = new System.Drawing.Size(150, 32);
            this.allUsersToolStripMenuItem.Text = "All Users";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::SchoolMG.Resource.about_48px;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(180, 32);
            this.aboutToolStripMenuItem.Text = "អំពីកម្មវិធី";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Image = global::SchoolMG.Resource.sign_out_48px;
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(180, 32);
            this.closeToolStripMenuItem.Text = "Log Out";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem1
            // 
            this.closeToolStripMenuItem1.Image = global::SchoolMG.Resource.icons8_shutdown_48px;
            this.closeToolStripMenuItem1.Name = "closeToolStripMenuItem1";
            this.closeToolStripMenuItem1.Size = new System.Drawing.Size(180, 32);
            this.closeToolStripMenuItem1.Text = "Close";
            this.closeToolStripMenuItem1.Click += new System.EventHandler(this.closeToolStripMenuItem1_Click);
            // 
            // classToolStripMenuItem
            // 
            this.classToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newClassToolStripMenuItem,
            this.ClassDetailToolStripMenuItem,
            this.courseToolStripMenuItem,
            this.roomToolStripMenuItem,
            this.timeToolStripMenuItem,
            this.teacherToolStripMenuItem});
            this.classToolStripMenuItem.Image = global::SchoolMG.Resource.classroom_48px;
            this.classToolStripMenuItem.Name = "classToolStripMenuItem";
            this.classToolStripMenuItem.Size = new System.Drawing.Size(65, 31);
            this.classToolStripMenuItem.Text = "ថ្នាក់";
            this.classToolStripMenuItem.Click += new System.EventHandler(this.classToolStripMenuItem_Click);
            // 
            // newClassToolStripMenuItem
            // 
            this.newClassToolStripMenuItem.Image = global::SchoolMG.Resource.icons8_add_48px;
            this.newClassToolStripMenuItem.Name = "newClassToolStripMenuItem";
            this.newClassToolStripMenuItem.Size = new System.Drawing.Size(180, 32);
            this.newClassToolStripMenuItem.Text = "បង្កើតថ្នាក់ថ្មី";
            this.newClassToolStripMenuItem.Click += new System.EventHandler(this.newClassToolStripMenuItem_Click);
            // 
            // ClassDetailToolStripMenuItem
            // 
            this.ClassDetailToolStripMenuItem.Image = global::SchoolMG.Resource.icons8_class_48px;
            this.ClassDetailToolStripMenuItem.Name = "ClassDetailToolStripMenuItem";
            this.ClassDetailToolStripMenuItem.Size = new System.Drawing.Size(180, 32);
            this.ClassDetailToolStripMenuItem.Text = "បញ្ជីរថ្នាក់";
            this.ClassDetailToolStripMenuItem.Click += new System.EventHandler(this.ClassDetailToolStripMenuItem_Click);
            // 
            // courseToolStripMenuItem
            // 
            this.courseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newCourseToolStripMenuItem,
            this.listCourseToolStripMenuItem});
            this.courseToolStripMenuItem.Name = "courseToolStripMenuItem";
            this.courseToolStripMenuItem.Size = new System.Drawing.Size(180, 32);
            this.courseToolStripMenuItem.Text = "Course";
            // 
            // newCourseToolStripMenuItem
            // 
            this.newCourseToolStripMenuItem.Name = "newCourseToolStripMenuItem";
            this.newCourseToolStripMenuItem.Size = new System.Drawing.Size(166, 32);
            this.newCourseToolStripMenuItem.Text = "New Course";
            // 
            // listCourseToolStripMenuItem
            // 
            this.listCourseToolStripMenuItem.Name = "listCourseToolStripMenuItem";
            this.listCourseToolStripMenuItem.Size = new System.Drawing.Size(166, 32);
            this.listCourseToolStripMenuItem.Text = "List Course";
            // 
            // roomToolStripMenuItem
            // 
            this.roomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newRoomToolStripMenuItem,
            this.listRoomToolStripMenuItem});
            this.roomToolStripMenuItem.Name = "roomToolStripMenuItem";
            this.roomToolStripMenuItem.Size = new System.Drawing.Size(180, 32);
            this.roomToolStripMenuItem.Text = "Room";
            // 
            // newRoomToolStripMenuItem
            // 
            this.newRoomToolStripMenuItem.Name = "newRoomToolStripMenuItem";
            this.newRoomToolStripMenuItem.Size = new System.Drawing.Size(157, 32);
            this.newRoomToolStripMenuItem.Text = "New Room";
            // 
            // listRoomToolStripMenuItem
            // 
            this.listRoomToolStripMenuItem.Name = "listRoomToolStripMenuItem";
            this.listRoomToolStripMenuItem.Size = new System.Drawing.Size(157, 32);
            this.listRoomToolStripMenuItem.Text = "List Room";
            // 
            // timeToolStripMenuItem
            // 
            this.timeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTimeToolStripMenuItem,
            this.listTimeToolStripMenuItem});
            this.timeToolStripMenuItem.Name = "timeToolStripMenuItem";
            this.timeToolStripMenuItem.Size = new System.Drawing.Size(180, 32);
            this.timeToolStripMenuItem.Text = "Time";
            // 
            // newTimeToolStripMenuItem
            // 
            this.newTimeToolStripMenuItem.Name = "newTimeToolStripMenuItem";
            this.newTimeToolStripMenuItem.Size = new System.Drawing.Size(150, 32);
            this.newTimeToolStripMenuItem.Text = "New Time";
            // 
            // listTimeToolStripMenuItem
            // 
            this.listTimeToolStripMenuItem.Name = "listTimeToolStripMenuItem";
            this.listTimeToolStripMenuItem.Size = new System.Drawing.Size(150, 32);
            this.listTimeToolStripMenuItem.Text = "List Time";
            // 
            // teacherToolStripMenuItem
            // 
            this.teacherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTeacherToolStripMenuItem,
            this.listTeacherToolStripMenuItem});
            this.teacherToolStripMenuItem.Name = "teacherToolStripMenuItem";
            this.teacherToolStripMenuItem.Size = new System.Drawing.Size(180, 32);
            this.teacherToolStripMenuItem.Text = "Teacher";
            // 
            // newTeacherToolStripMenuItem
            // 
            this.newTeacherToolStripMenuItem.Name = "newTeacherToolStripMenuItem";
            this.newTeacherToolStripMenuItem.Size = new System.Drawing.Size(172, 32);
            this.newTeacherToolStripMenuItem.Text = "New Teacher";
            // 
            // listTeacherToolStripMenuItem
            // 
            this.listTeacherToolStripMenuItem.Name = "listTeacherToolStripMenuItem";
            this.listTeacherToolStripMenuItem.Size = new System.Drawing.Size(172, 32);
            this.listTeacherToolStripMenuItem.Text = "List Teacher";
            // 
            // studentToolStripMenuItem
            // 
            this.studentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStudentToolStripMenuItem,
            this.listStudentToolStripMenuItem});
            this.studentToolStripMenuItem.Image = global::SchoolMG.Resource.student_male_48px;
            this.studentToolStripMenuItem.Name = "studentToolStripMenuItem";
            this.studentToolStripMenuItem.Size = new System.Drawing.Size(75, 31);
            this.studentToolStripMenuItem.Text = "សិស្ស";
            // 
            // newStudentToolStripMenuItem
            // 
            this.newStudentToolStripMenuItem.Image = global::SchoolMG.Resource.icons8_add_48px;
            this.newStudentToolStripMenuItem.Name = "newStudentToolStripMenuItem";
            this.newStudentToolStripMenuItem.Size = new System.Drawing.Size(187, 32);
            this.newStudentToolStripMenuItem.Text = "ចុះឈ្មោះសិស្ស";
            this.newStudentToolStripMenuItem.Click += new System.EventHandler(this.newStudentToolStripMenuItem_Click);
            // 
            // listStudentToolStripMenuItem
            // 
            this.listStudentToolStripMenuItem.Image = global::SchoolMG.Resource.icons8_list_48px;
            this.listStudentToolStripMenuItem.Name = "listStudentToolStripMenuItem";
            this.listStudentToolStripMenuItem.Size = new System.Drawing.Size(187, 32);
            this.listStudentToolStripMenuItem.Text = "បញ្ជីរឈ្មោះសិស្ស";
            this.listStudentToolStripMenuItem.Click += new System.EventHandler(this.listStudentToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.របយករណលកToolStripMenuItem,
            this.StudentSellRptToolStripMenuItem});
            this.reportToolStripMenuItem.Image = global::SchoolMG.Resource.view_details_48px;
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(116, 31);
            this.reportToolStripMenuItem.Text = "របាយការណ៍";
            // 
            // របយករណលកToolStripMenuItem
            // 
            this.របយករណលកToolStripMenuItem.Image = global::SchoolMG.Resource.icons8_list_48px;
            this.របយករណលកToolStripMenuItem.Name = "របយករណលកToolStripMenuItem";
            this.របយករណលកToolStripMenuItem.Size = new System.Drawing.Size(195, 32);
            this.របយករណលកToolStripMenuItem.Text = "របាយការណ៍លក់";
            this.របយករណលកToolStripMenuItem.Click += new System.EventHandler(this.របយករណលកToolStripMenuItem_Click);
            // 
            // StudentSellRptToolStripMenuItem
            // 
            this.StudentSellRptToolStripMenuItem.Image = global::SchoolMG.Resource.spreadsheet_48px;
            this.StudentSellRptToolStripMenuItem.Name = "StudentSellRptToolStripMenuItem";
            this.StudentSellRptToolStripMenuItem.Size = new System.Drawing.Size(195, 32);
            this.StudentSellRptToolStripMenuItem.Text = "របាយការណ៍សិស្ស";
            this.StudentSellRptToolStripMenuItem.Click += new System.EventHandler(this.StudentSellRptToolStripMenuItem_Click);
            // 
            // pnlMian
            // 
            this.pnlMian.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMian.Location = new System.Drawing.Point(0, 114);
            this.pnlMian.Name = "pnlMian";
            this.pnlMian.Size = new System.Drawing.Size(1264, 567);
            this.pnlMian.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(65)))), ((int)(((byte)(81)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnBack);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1264, 75);
            this.panel1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Khmer OS Battambang", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(947, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 34);
            this.label1.TabIndex = 2;
            this.label1.Text = "ស្វាគមន៍មកកាន់ម៉ាស្ទ័រអាយធី";
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.Transparent;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(100)))), ((int)(((byte)(113)))));
            this.btnBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(100)))), ((int)(((byte)(113)))));
            this.btnBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(100)))), ((int)(((byte)(113)))));
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.Location = new System.Drawing.Point(8, 16);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(41, 41);
            this.btnBack.TabIndex = 1;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1192, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Home
            // 
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.pnlMian);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Khmer OS Battambang", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.MaximizedBoundsChanged += new System.EventHandler(this.Home_MaximizedBoundsChanged);
            this.MaximumSizeChanged += new System.EventHandler(this.Home_MaximumSizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Home_FormClosing);
            this.Load += new System.EventHandler(this.Home_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newClassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ClassDetailToolStripMenuItem;
        private System.Windows.Forms.Panel pnlMian;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem របយករណលកToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StudentSellRptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem courseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newCourseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listCourseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newRoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listRoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teacherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTeacherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listTeacherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listStudentToolStripMenuItem;
    }
}