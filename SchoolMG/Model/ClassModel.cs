﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.Model
{
    public class ClassModel
    {
        public int ClassId { get; set; }
        public int CourseId { get; set; }
        public int TimeId { get; set; }
        public int Turn { get; set; }
        public int RoomId { get; set; }
        public int TeacherId { get; set; }
        public int Status { get; set; }
        public DateTime StartClassDate { get; set; }
        public DateTime EndClassDate { get; set; }

        public CourseModel CourseModel { get; }
        
    }
}
