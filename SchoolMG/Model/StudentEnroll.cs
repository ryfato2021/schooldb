﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.Model
{
    class StudentEnroll
    {
        public int EnrollId { get; set; }
        public int ClassId { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string Phone { get; set; }
        public DateTime EnrollDate { get; set; }
        public string StudentStatus { get; set; }
    }
}
