﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.Model
{
    public class StudentReportDetail
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public DateTime EnrollDate { get; set; }
        public string StudentStatus { get; set; }
        public string PaymentStatus { get; set; }
        public double Owed { get; set; }
    }
}
