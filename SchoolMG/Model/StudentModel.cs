﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.Model
{
    public class StudentModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte Gender { get; set; }
        public DateTime Dob { get; set; }
        public string Phone { get; set; }
        public string Facebook { get; set; }
        public string Telegram { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhotoPath { get; set; }
    }
}
