﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.Model
{
    public class SellReport
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string CourseName { get; set; }
        public double PaidAmount { get; set; }
        public double Owed { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime PaidDate { get; set; }
        public string Cashier { get; set; }
    }
}
