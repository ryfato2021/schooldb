﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolMG.Model
{
    public class PaymentModel
    {
        public int StudentId { get; set; }
        public int ClassId { get; set; }
        public int StudentStatus { get; set; }

        public double Discount { get; set; }
        public double  PaidAmount { get; set; }
        public string Note { get; set; }
    }
}
